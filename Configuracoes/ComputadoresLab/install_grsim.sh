#!/usr/bin/sh
# Instala o grSim

mkdir -p $HOME/SSL
cd $HOME/SSL/
git clone https://github.com/RoboCup-SSL/grsim.git
cd grsim
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local/bin ..
make
cp ../bin/grsim $HOME/.local/bin/
