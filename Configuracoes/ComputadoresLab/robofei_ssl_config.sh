#!/bin/sh
# Instalação das depêndencias para o projeto RoboFEI - SSL - Ubuntu/Mint

sudo apt install -y \
    build-essential git neovim nodejs luarocks autoconf ripgrep \
    sxiv fish curl flameshot zathura texlive-full gimp inkscape ipe npm cmake \
    clang clangd fd-find asciidoctor neofetch meld figlet \
    lsp-plugins valgrind libeigen3-dev pybind11-dev python3-pip python3-tk \
    stow libdlib-dev libopenblas-dev liblapack-dev mesa-common-dev kitty

# Instalação do QtCreator
sudo apt install -y qtcreator qt5-doc qt5-doc-html \
    qtbase5-doc-html qtbase5-examples libqt5network5 libqt5serialport5-dev \
    qtmultimedia5-dev libqt5gamepad5-dev

# Depêndencias do simulador da liga
sudo apt install -y pkg-config libqt5opengl5-dev libgl1-mesa-dev \
    libglu1-mesa-dev libprotobuf-dev protobuf-compiler libode-dev libboost-dev

pip install numpy Pillow sklearn twine wheel setuptools console_progressbar \
    compiledb

# Muda o shell padrão para o Fish
chsh -s /usr/bin/fish

# Adiciona o usuário ao grupo dialout, necessário para usar a porta serial
sudo adduser $USER dialout

# Fontes
cd
mkdir $HOME/.fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip
unzip JetBrainsMono.zip -d JetBrainsMono/
mv JetBrainsMono/* ~/.fonts
fc-cache -v
rm -r $HOME/JetBrains*
