# Overlay do RoboFEI para livestreams

## Uso

A pasta `Dados` contém alguns arquivos de texto que controlam o texto do nome
da equipe e o placar.

Como o OBS exporta os caminhos completos da máquina que gerou a configuração, é
necessário corrigir o caminho para esses arquivos de texto quando abrir o OBS
pela primeira vez. Para fazer isso basta ir na `Cena: Jogo` e alterar o campo
`Text File` das seguintes fontes:

- Score_amarelo
- Time_amarelo
- Score_azul
- Time_azul

Na `Cena: Intro` também é utilizado um arquivo de texto para mostrar os
resultados dos jogos do dia, o processo é o mesmo. A `Cena: Out` também mostra
o mesmo arquivo, mas, ela usa uma cópia, que é atualizada automaticamente.
