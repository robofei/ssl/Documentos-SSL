# Documentos RoboFEI - Small Size

Este repositório possui os treinamentos utilizados para novos membros, manuais
de todas as áreas e alguns documentos diversos.

## Treinamentos

### Programação

- [Informações Básicas](https://robofei.gitlab.io/ssl/Documentos-SSL/CPP_101)
- [Atividade 0](https://robofei.gitlab.io/ssl/Documentos-SSL/QT)
- [Atividade 1](https://robofei.gitlab.io/ssl/Documentos-SSL/Agenda)
- [Atividade 2](https://robofei.gitlab.io/ssl/Documentos-SSL/Calculadora)
- [Atividade 3](https://robofei.gitlab.io/ssl/Documentos-SSL/Tetris)

### Mecânica

- [Manual de Funcionamento do Robô](https://robofei.gitlab.io/ssl/Documentos-SSL/Roteiro_funcionamento_ssl)


### DevOps

- [Microsoft Azure DevOps - Guia de Uso](https://gitlab.com/robofei/ssl/Documentos-SSL/-/raw/master/Treinamentos/devops/devops.pdf?ref_type=heads&inline=false)


## Tutoriais

### Programação

- [Conexão com o rádio](https://robofei.gitlab.io/ssl/Documentos-SSL/conexao_radio)
- [Partida de simulação](https://robofei.gitlab.io/ssl/Documentos-SSL/partida_simulacao)