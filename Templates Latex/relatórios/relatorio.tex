\documentclass[12pt,letterpaper]{article}
\usepackage{fullpage}
\usepackage[top=2cm, bottom=4.5cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb,amscd}
\usepackage{lastpage}
\usepackage{enumerate}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{times}
\usepackage{listings}
\usepackage{color}
\usepackage{subfigure}
\usepackage{amsmath} 	

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\definecolor{lightgray}{rgb}{0.95,0.95,0.95}


\lstset{frame=tb,
	language=Python,
	aboveskip=3mm,
	belowskip=3mm,
	showstringspaces=false,
	columns=flexible,
	basicstyle={\small\ttfamily},
	numbers=none,
	numberstyle=\tiny\color{gray},
	keywordstyle=\color{blue},
	commentstyle=\color{dkgreen},
	stringstyle=\color{mauve},
	breaklines=true,
	breakatwhitespace=true,
	frame=shadowbox,
	tabsize=3,
	backgroundcolor=\color{lightgray}
}

%%
\usepackage[ruled,linesnumbered,noend]{algorithm2e}
\SetKwProg{Fn}{procedure}{}{end}\SetKwFunction{FRecurs}{FnRecursive}%
%%

\usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\fontfamily{Times New Roman}

\hypersetup{%
  colorlinks=true,
  linkcolor=blue,
  linkbordercolor={0 0 1}
}
 
\renewcommand\lstlistingname{Algorithm}
\renewcommand\lstlistlistingname{Algorithms}
\def\lstlistingautorefname{Alg.}

\lstdefinestyle{Python}{
    language        = Python,
    frame           = lines, 
    basicstyle      = \footnotesize,
    keywordstyle    = \color{blue},
    stringstyle     = \color{green},
    commentstyle    = \color{red}\ttfamily
}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.05in}

% Edit these as appropriate
\newcommand\hwnumber{[PEL215] - }                  % <-- homework number
\newcommand\NetIDa{netid19823}           % <-- NetID of person #1
%\newcommand\NetIDb{netid12038}           % <-- NetID of %person #2 (Comment this line out for problem sets)

\pagestyle{fancyplain}
\headheight 35pt
\lhead{Guilherme Luis Pauli - 123105-9 \\ \textbf{\centering \small \hwnumber Robótica Móvel }}
\rhead{\small \textbf{Atividade 3} \\ \today}

\lfoot{}
\cfoot{}
\rfoot{\small\thepage}
\headsep 1.5em

\begin{document}

\begin{Large}
	{Localização de Robôs Móveis - Filtro de Kalman (1D).}
\end{Large}

\section{Introdução}

Este relatório vai abordar o conceito de Localização de robôs móveis utilizando a técnica de computacional de Filtro de Kalman [\textit{Kalman Filter} - KF, em inglês]. O objetivo desta atividade é realizar a implementação do KF para fornecer a covariância do robô ao longo de seu deslocamento em apenas um eixo (X) do mapa/ambiente definido. O robô deverá andar em linha reta com velocidade contínua, enquanto o KF realiza predições e atualiza a covariância do robô acerca da certeza de sua posição no mapa. 

O mapa é conhecido (Figura \ref{ref:arena}), logo a posição das portas também. Ao detectar uma porta o Filtro de Kalman realiza a etapa de \textit{Atualização} com a medida da posição da porta (pois o robô acaba de cruzar esta posição) e recalcula sua covariância, ou seja, sua confiança de que está na posição calculada.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.0\linewidth]{figs/kalman.PNG}
\caption{Imagem da arena do desafio desta atividade.}
\label{ref:arena}
\end{figure}

As etapas de \textit{Predição} acontecem sempre, já as de \textit{Atualização} somente quando o robô recebe uma nova leitura dos sensores (i.e., quando uma porta é detectada). Seguindo estes passos até o fim do trajeto é possível observar no desenho da figura \ref{ref:graf} a incerteza crescer exponencialmente até que uma nova medição pelos sensores seja realizada e sua posição corrigida.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\linewidth]{figs/Myfig.PNG}
\caption{Cálculo da covariância fornecida pelo KF durante o trajeto do robô.}
\label{ref:graf}
\end{figure}

As equações que irão descrever o modelo do robô são descritas pelas equações de movimento da física, dadas por \ref{eq:posx} e \ref{eq:velx}.

\begin{equation}
	\label{eq:posx}
	X_k = X_{(k-1)} + \Dot{X}_{(k-1)}{\Delta{t}} + \frac{1}{2}{a_{x}}\Delta{t}^{2}
\end{equation}

\begin{equation}
	\label{eq:velx}
	\Dot{X}_k = \Dot{X}_{(k-1)} +  {a_{x}}\Delta{t}
\end{equation}


\section{Filtro de Kalman}

O Filtro de Kalman Discreto (DKF, em inglês) é um recurso matemático que visa o ótimo, uma técnica de estimar estados elaborada em 1960 por Rudolf E. Kalman sendo comumente utilizado em navegação robótica e sistemas dinâmicos lineares contaminados de ruido branco Gaussiano. Ele funciona através de equações estatísticas e do modelo do objeto em estudo, quantificando o erro das medições provenientes de sensores $(R)$ e as estimadas no processo $(Q)$, balanceando as incertezas através do ganho de Kalman. Seu funcionamento é regido por duas fases chamadas de Predição e Atualização, ou ainda \textit{a priori} e \textit{a posteriori}, respectivamente. Sendo, nas equações dispostas abaixo, todas as variáveis matrizes, {k} a referência para o estado atual e {k-1} para o estado anterior, ou também \textit{a priori} e \textit{a posteriori}, respectivamente, e a notação $\hat{X}$ representa uma estimativa.

\begin{flalign} \label{eqKalman1}
	\hat{X}_{k|k-1} = F\hat{X}_{k-1|k-1} + Bu_{k} &&
\end{flalign}
\begin{flalign} \label{eqKalman2}
	P_{k|k-1}       = FP_{k-1|k-1}F^T + Q &&
\end{flalign}
\begin{flalign} \label{eqKalman3}
	Y_{k|k} = Z_{k|k} - H\hat{X}_{k|k-1}&&
\end{flalign}
\begin{flalign} \label{eqKalman45}
	K_{k|k} =  \frac{P_{k|k-1}H^T}{HP_{k|k-1}H^T + R} &&
\end{flalign}
\begin{flalign} \label{eqKalman6}
	\hat{X}_{k|k} = \hat{X}_{k|k-1} +K_kY_{k|k} &&
\end{flalign}
\begin{flalign} \label{eqKalman7}
	P_{k|k} = (I - K_{k|k}H)P_{k|k-1} &&
\end{flalign}

O filtro começa na fase de \textit{Predição} estimando as variáveis de estado $(\hat{X}_k)$ em \ref{eqKalman1} com os valores iniciais da suas variáveis e então é calculado o erro da predição dos estados $(P_k)$ feita em \ref{eqKalman2}, normalmente a matriz $(P_k)$ é iniciada com valores na diagonal principal igual a 1\textit{1.0} quando os valores iniciais das variáveis de estado não são conhecidos, porém, no caso desta atividade, a posição inicial é conhecida e, portanto, esta matriz iniciada com valores \textit{0.0}.
Na segunda etapa, de \textit{Atualização}, é inserido uma nova amostra das variáveis de estado $(Z_k)$ para que, através do ganho de Kalman $(K_k)$, o sistema comece a convergir para o valor real. Após algumas iterações o ganho se estabiliza e atinge o ótimo para as variáveis de estado observadas. As matrizes $(F)$,$(B)$ e $(u)$ são dadas pela representação em espaço de estados das eq.\ref{eq:posx} e \ref{eq:velx}.

Os valores iniciais utilizados nos cálculos foram: 

\begin{itemize}
	\item \textbf{$\Delta$t:} (timestep/1000) --> 0.032 $ms$
	\item \textbf{u:} 0.0 $m/s^2$
	\item \textbf{Matriz X:} 
	$\begin{bmatrix}
		-4.0 \\
		0.2 
	\end{bmatrix}$
	$\begin{bmatrix}
		m \\
		m/s 
	\end{bmatrix}$	
	\item \textbf{Matriz F:} 
	$\begin{bmatrix}
		1.0 & \Delta t \\
		0.0 & 1.0 
	\end{bmatrix}$
	\item \textbf{Matriz B:} 
	$\begin{bmatrix}
		\frac{\Delta t^2}{2} \\
		\Delta t
	\end{bmatrix}$
	\item \textbf{Matriz P:} 
	$\begin{bmatrix}
		0.0 & 0.0 \\
		0.0 & 0.0 
	\end{bmatrix}$
	\item \textbf{Matriz R:} 	$\begin{bmatrix} 0.001	\end{bmatrix}$
	\item \textbf{Matriz Q:} 
	$\begin{bmatrix}
		0.0001 & 0.0 \\
		0.0 & 0.0001 
	\end{bmatrix}$
	\item \textbf{Matriz H:} 
	$\begin{bmatrix}
		1.0 & 0.0
	\end{bmatrix}$

\end{itemize}
 

\section{{Código Fonte}}

Abaixo, segue o código comentado do Filtro de Kalman implementado para o \textit{TurtleBot} em C++ e o código para gerar o vídeo e gráficos apresentados na sessão \ref{video}.

\begin{lstlisting}[title=Filtro de Kalman 1D para o robô TurtleBot.]
// File:          my_controller.cpp
// Date:
// Description:
// Author:
// Modifications:

// You may need to add webots include files such as
// <webots/DistanceSensor.hpp>, <webots/Motor.hpp>, etc.
// and/or to add some other includes

// #include <algorithm>
#include <cmath>
#include <cstdio>
#include <math.h>
#include <vector>
#include <webots/Robot.hpp>
#include <webots/Motor.hpp>
#include <webots/Supervisor.hpp>
#include <webots/Lidar.hpp>
#include <Eigen/Dense>
#include <IO.h>

// All the webots classes are defined in the "webots" namespace
using namespace webots;


std::string sep = "\n----------------------------------------\n";
Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "\t [", "]");

// This is the main program of your controller.
// It creates an instance of your Robot instance, launches its
// function(s) and destroys it at the end of the execution.
// Note that only one instance of Robot should be created in
// a controller program.
// The arguments of the main function can be specified by the
// "controllerArgs" field of the Robot node

// Classe para movimentacao do robo omnidirecional
class KalmanFilter
{    
	double x;  // posicao do robo
	double v;  // velocidade do robo
	double dt; // tempo que se passou entre cada timestamp
	double u;  // variavel de controle
	
	Eigen::Matrix<double, 2, 2> F; // Matriz de transicao de estados
	Eigen::Matrix<double, 2, 1> B; // Matriz das entradas de controle
	Eigen::Matrix<double, 1, 1> Y; // Matriz 
	Eigen::Matrix<double, 2, 1> K; // Ganho de Kalman
	Eigen::Matrix<double, 2, 2> P; // Predicao da Covariancia
	Eigen::Matrix<double, 1, 2> H; // Matriz do modelo de observacao de estados
	Eigen::Matrix<double, 2, 2> Q; // Covariancia do erro/ruido do modelo/processo
	Eigen::Matrix<double, 1, 1> R; // Covariancia do erro/ruido dos sensores
	Eigen::Matrix<double, 1, 1> S; // Covariancia do erro/ruido dos sensores
	
	Eigen::MatrixXd I = Eigen::Matrix<double,2,2>::Identity();
	
	Eigen::Matrix<double, 2, 1> X; // Pose do Robo [m]
	//Eigen::Matrix<double, 1, 1> z; // Nova medicao da variavel de estado do robo
	
	public:
	/**
	* Construtor da classe
	*
	*/
	KalmanFilter(double _x, double _v)
	{
		F << 1.0, 0.032,
		0.0, 1.0;
		
		B << (dt*dt)/2.f,
		dt;
		
		P << 0.0, 0.0,
		0.0, 0.0;
		
		Q << 0.0001, 0.00,
		0.00, 0.0001;
		
		R << 0.00;
		
		H << 1.0, 0.0;
		
		x = _x;
		v = _v;
		
		u = 0.0;
		
		X << x,
		v;
	}
	
	// Funcao que realiza a etapa de predicao do KF
	void predict(){
		
		// calculo da projecao das variaveis de estado um timestep a frente (a priori)
		X = F*X + B*u;
		// calculo da projecao da covariancia do erro 
		P = F*P*F.transpose() + Q;
	}
	
	// Funcao que realiza a etapa de atualizacao do KF
	void update(Eigen::Matrix<double, 1, 1> z){
		// calculo do erro da estimativa
		// considerando uma nova medicao das variaveis de estado
		Y = z - H*X;
		// residuo da covariancia
		S = H*P*H.transpose() + R;
		// calculo do ganho de Kalman
		K = P*H.transpose()*S.inverse();
		// atualizacao das variaveis de estado (a posteriori)
		X = X + K*Y;
		// atualizacao da estmativa da covariancia do erro 
		P = (I - K*H)*P;
		
		X[1] = 0.2;
	}
	
	void setDT(double _dt){
		dt = _dt;
	}    
	
	Eigen::Matrix<double,2,1> getPose(){
		return X;
	}
	
	Eigen::Matrix<double,2,2> getP_Matrix(){
		return P;
	} 
	
};

class rbtTurtle
{
	
	webots::Motor *leftMotor;
	webots::Motor *rightMotor;
	
	public:
	/**
	* Construtor da classe
	*
	*/
	rbtTurtle(Robot *_rbt)
	{
		// Obtem os motores do Turtlebot
		leftMotor = _rbt->getMotor("left wheel motor");
		rightMotor = _rbt->getMotor("right wheel motor");
		leftMotor->setPosition(INFINITY);
		rightMotor->setPosition(INFINITY);
	}
	
	// Define a velocidade dos motores
	void setSpeed(double _speed)
	{
		leftMotor->setVelocity(_speed);  // Velocidade esquerda
		rightMotor->setVelocity(_speed); // Velocidade direita
	}
	
	void showLidarPoints(const LidarPoint* _points, int _nPoints)
	{
		std::cout << "\n\n";
		for (int n = 0; n < _nPoints/2; n++)
		{
			std::cout << "(" << _points[n].x << ", " << _points[n].y << ")  ";
		}
		std::cout << "------------------------------------------------\n";
	}
	
};

class MotionPath
{
	int iDoorNumber;
	std::vector<double> vtDoorPose;
	
	public:
	/**
	* Construtor da classe
	*
	*/
	MotionPath(int inb){
		iDoorNumber = inb;
		vtDoorPose.push_back(-2.75);
		vtDoorPose.push_back(-0.75);
		vtDoorPose.push_back(2.75);
	}
	
	int getDoorNb(){
		return iDoorNumber;
	}
	
	void updateDoorNb(){
		iDoorNumber += 1;
		
		if(iDoorNumber > 2)
		iDoorNumber = 2;
	}
	
	Eigen::Matrix<double, 1, 1> dDoorPose(int iNb){  
		Eigen::Matrix<double, 1, 1> z;
		z << vtDoorPose.at(iNb);
		return z;
	}
};

int main(int argc, char **argv) {
	// create the Robot instance.
	Robot *robot = new Robot();
	
	// Instanciando o objeto da classe do robo turtle
	rbtTurtle *rbtTT = new rbtTurtle(robot);
	rbtTT->setSpeed(0.2/0.033); // Velocidade dada em radianos, dado que v=0.2 [m/s] e r=0.033 [m]
	
	// Instanciando o objeto da classe do Filtro de Kalman
	KalmanFilter *kf = new KalmanFilter(-4.0, 0.2);
	
	// Instanciando o objeto da classe MotionPath
	MotionPath *mp = new MotionPath(0);
	std::cout<< "door nb: " << mp->getDoorNb();
	
	// get the time step of the current world.
	int timeStep = (int)robot->getBasicTimeStep();
	
	// get and enable the lidar
	Lidar *rbtLidar = robot->getLidar("LDS-01");
	rbtLidar->enable(timeStep);
	rbtLidar->enablePointCloud();
	
	double dt_s = timeStep/1000.f;
	kf->setDT(dt_s);
	bool bFlagInit = false;
	bool bFlagFim = false;
	
	std::string sep = "\n----------------------------------------\n";
	Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "\t [", "]");
	
	// Arquivo binario com os dados da distribuicao do robo gerados pelo Filtro de Kalman
	std::FILE *motionData; 
	motionData = fopen("motion_data.bin", "wb");
	
	// Main loop:
	// - perform simulation steps until Webots is stopping the controller
	while (robot->step(timeStep) != -1) {
		
		// Obtenha leituras do sensor   
		const LidarPoint* points = rbtLidar->getPointCloud();
		int nPoints = rbtLidar->getNumberOfPoints();
		
		// variaveis para guardar a posicao do robo e covariancia estimada    
		Eigen::Matrix<double,2,1> Pose;
		Eigen::Matrix<double,2,2> PMatrix;
		
		// Recuperando os valores da Pose do Robo e 
		// da Covariancia gerados pelo Filtro de Kalman
		Pose = kf->getPose();
		PMatrix = kf->getP_Matrix();
		
		// Escrevendo no arquivo binario
		double media = Pose[0], desvio = PMatrix(0,0);
		fwrite(&media, sizeof(media), 1, motionData);
		fwrite(&desvio, sizeof(desvio), 1, motionData);
		
		// Rodando sempre a etapa de predicao do Filtro de Kalman
		// para obter uma estimativa da Pose do robo
		kf->predict();
		
		// Avalia se o Lidar detectou uma porta
		// Se sim, chama a atualizacao do KF com a coordenada da nova porta
		if( isinf(points[107].x) && isinf(points[71].x) ) 
		{
			bFlagInit = true;
		}
		
		if(bFlagInit){
			if( !isinf(points[107].x) || !isinf(points[71].x) )
			{
				bFlagFim = true;
			}
			
			if(bFlagInit && bFlagFim)
			{
				std::cout << " UPDATING ! \n\n";
				std::cout<< " door nb: " << mp->getDoorNb() << "\n";
				
				Eigen::Matrix<double, 1, 1> _z;
				_z << mp->dDoorPose(mp->getDoorNb());
				
				std::cout<< " door val: " << _z << "\n";
				
				kf->update(_z);
				mp->updateDoorNb();
				
				bFlagInit = false;
				bFlagFim = false;
			}
		}
		
		//rbtTT->showLidarPoints(points,nPoints);
		
		if(Pose[0] > 4.0)
		{
			rbtTT->setSpeed(0.0); // Para o Robo
			fclose(motionData);  // Para a gravacao do arquivo
		}
		
		std::cout << sep;
		std::cout << "Pose \t( "<< Pose[0] << " , " << Pose[1] <<" ) \n";
		std::cout << "P "<< PMatrix.format(CleanFmt) << " \n";
		
	};
	
	// Enter here exit cleanup code.
	delete robot;
	return 0;
}
\end{lstlisting}

\begin{lstlisting}[title=Código em Python para gerar o gráfico animado.]
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegWriter, PillowWriter
import scipy.stats as stats
import math


# Change to reflect your file location!
plt.rcParams['animation.ffmpeg_path'] = 'C:\\Codigos Aulas Mestrado\\PEL215\\ffmpeg\\bin\\ffmpeg.exe'

metadata = dict(title='Kalman Filter 1D', artist='pauli')
writer = FFMpegWriter(fps=31, metadata=metadata) #15
singaxis = FFMpegWriter(fps=32, metadata=metadata) #15

fig, axs = plt.subplots(2)
fig.suptitle('Densidade de Probabilidade')
fig.supxlabel('Posicao [m]')

def normalDist(mu,sigma):
return np.linspace(mu - 3*sigma, mu + 3*sigma, 100)

# Variaveis de Distribuicao de Probabilidade de cada porta conhecida no ambiente
# Grafos estaticos
st_mean = [-2.75, -0.75, 2.75]
st_var = 0.17
st_sigma = math.sqrt(st_var)

if __name__ == "__main__":
data = np.fromfile('C:/Codigos Aulas Mestrado/PEL215/motion_data.bin', dtype=float)
media = data[::2]  # pega todos os indices pares
desvio = data[1::2] # pega todos os indices impares

print("tam Media: ",len(media)," || tam desvio: ", len(desvio))
count = 1
lines = []
labels = []
for st in st_mean:
x_st = normalDist(st,st_sigma)
door_pdf = stats.norm.pdf(x_st,st,st_sigma)
axs[0].plot(x_st, door_pdf, label=f'Porta n{count}')
axs[0].plot([st,st],[0,np.max(door_pdf)],'k--')
axs[0].fill_between(x_st, door_pdf, alpha=0.5)
axs[0].set_xlim(-4.5, 4.5)
axs[0].set_ylim(0, 1.0)
axs[0].annotate(f'Porta n{count}', xy=(st, np.max(door_pdf)), xytext=(-25, 7),
textcoords='offset points', ha='left', va='center')
count = count + 1
plt.tight_layout()
plt.cla()

# Remove as molduras dos subplots
for ax in axs:
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_linewidth(1)  # Ajusta a largura da linha do eixo X
ax.spines['left'].set_linewidth(1)    # Ajusta a largura da linha do eixo Y

# para o segundo grafo (robo)
variance = np.linspace(0,1,100)
mu = np.linspace(-4,4,100)
sigma = []

bdone = False
bdone2 = False
bdone3 = False
with writer.saving(fig, "test.mp4", 100):
for i in range(0,round(len(media))):
if(desvio[i] > 0.55):
desvio[i] = 0.5
if desvio[i] <= 0:
desvio[i] = 0.01

sigma.append(math.sqrt(desvio[i]))
print(media[i],sigma[i])
pdf = stats.norm.pdf(normalDist(media[i],sigma[i]), media[i], sigma[i])
max_pt = np.max(pdf) 
axs[1].plot([media[i],media[i]],[0,max_pt],'k--')
axs[1].plot(normalDist(media[i],sigma[i]), pdf)
axs[1].fill_between(normalDist(media[i],sigma[i]), pdf, color='skyblue', alpha=0.5, label='Area Preenchida')
axs[1].scatter(media[i], 0, color='purple', marker='o',s=49)    
axs[1].set_xlim(-4.5, 4.5)
axs[1].set_ylim(0, 2)
axs[1].annotate(f'Pose Robo: \n({media[i]:.2f}, 0.2)',
xy=(media[i], 0), xycoords='data',
xytext=(-70, 70), textcoords='offset points',
arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.4"))
axs[1].plot([-5,-3,-3],[1,1,0],':',color='gray')
axs[1].fill_between([-5,-3,-3],[1,1,0], color='gray', alpha=0.1)
axs[1].plot([-2.5,-2.5,-1,-1],[0,1,1,0],':',color='gray')
axs[1].fill_between([-2.5,-2.5,-1,-1],[0,1,1,0], color='gray', alpha=0.1)
axs[1].plot([-0.5,-0.5,2.5,2.5],[0,1,1,0],':',color='gray')
axs[1].fill_between([-0.5,-0.5,2.5,2.5],[0,1,1,0], color='gray', alpha=0.1)
axs[1].plot([3,3,5,5],[0,1,1,0],':',color='gray')
axs[1].fill_between([3,3,5,5],[0,1,1,0], color='gray', alpha=0.1)

if media[i] >= st_mean[0] and sigma[i] < 0.2:
axs[0].fill_between(normalDist(st_mean[0],st_sigma), door_pdf, color='lightgray', alpha=0.5)
if not(bdone):
axs[0].annotate(f'Updating(z[{media[i]:.2f}])',
xy=(media[i], 0), xycoords='data',
xytext=(-80, -25), textcoords='offset points',
arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.1"))
bdone = True
if media[i] >= st_mean[1] and sigma[i] < 0.2:
axs[0].fill_between(normalDist(st_mean[1],st_sigma), door_pdf, color='lightgray', alpha=0.5)
if not(bdone2):
axs[0].annotate(f'Updating(z[{media[i]:.2f}])',
xy=(media[i], 0), xycoords='data',
xytext=(-20, -25), textcoords='offset points',
arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.4"))
bdone2 = True
if media[i] >= st_mean[2] and sigma[i] < 0.2:
axs[0].fill_between(normalDist(st_mean[2],st_sigma), door_pdf, color='lightgray', alpha=0.5)
if not(bdone3):
axs[0].annotate(f'Updating(z[{media[i]:.2f}])',
xy=(media[i], 0), xycoords='data',
xytext=(-60, -25), textcoords='offset points',
arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.4"))
bdone3 = True

writer.grab_frame()
plt.cla()
\end{lstlisting}

\section{Realizando o desafio [Vídeo]}\label{video}

O vídeo com o robô realizando a tarefa no circuito da arena pode ser visualizado acessando o link abaixo:

\url{https://youtu.be/IqGxu6_QASM}

\end{document}