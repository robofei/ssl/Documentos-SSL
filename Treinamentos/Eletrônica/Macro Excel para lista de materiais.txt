Sub group()

With ActiveSheet
m = 1
counter = 1
For n = 2 To 315
    partname = .Cells(n - 1, 2)
    If counter = 1 Then
        refs = .Cells(n - 1, 1)
    End If
    If .Cells(n, 2) = .Cells(n - 1, 2) Then
        counter = counter + 1
        refs = refs & ", " & .Cells(n, 1)
    Else
        partname = .Cells(n - 1, 2)
        .Cells(m, 8) = counter
        .Cells(m, 9) = partname
        .Cells(m, 10) = refs
        m = m + 1
        counter = 1
        refs = ""
    End If
Next

End With

End Sub
