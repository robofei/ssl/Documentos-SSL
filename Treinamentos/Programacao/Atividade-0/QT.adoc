= Atividade 0 - QtCreator
:imagesdir: ./Imagens/
:source-highlighter: rouge
include::attributes-pt.adoc[]
:xrefstyle: short
:docinfo1:
:sectnums:
:toc:
:experimental:

== Introdução

Nesta atividade inicial iremos conhecer o Qt Creator, que é o software
utilizado para programar o código da estratégia.

== Objetivo

O objetivo é instalar o Qt (caso não esteja), aprender como criar um novo
projeto e conhecer algumas funcionalidades úteis para o desenvolvimento do
código.

== Instalação do Qt Creator

=== Download

O primeiro passo é fazer o download do instalador, ele pode ser encontrado no
https://www.qt.io/product/development-tools[site do Qt]. Após entrar no site clique
no botão para fazer o download (<<img-dload1>>).

[[img-dload1]]
.Download do Qt
image::download_1.png[align=center, title-align=center, width=80%]

Depois disso desça toda a página até achar o download para _open source_
(<<img-dload2>>), dessa forma, o software pode ser usado de graça.

[[img-dload2]]
.Download do Qt - 2
image::download_2.png[align=center, title-align=center, width=40%]

Então, uma página similar à esta (<<img-dload3>>) deve aparecer, o site irá
detectar seu sistema operacional e a versão correta para download (32 ou 64
bits), agora é só baixar.

[[img-dload3]]
.Download do Qt - 3
image::download_3.png[align=center, title-align=center, width=40%]

=== Instalação

Para instalar no linux, provavelmente será necessário permitir que o instalador
seja executado como um programa, para fazer isso basta clicar com o botão
direito no arquivo do instalador, ir em *Propriedades* -> *Permissões*
(<<img-setup0>>).

[[img-setup0]]
.Permitindo execução como um programa
image::setup_0.png[align=center, title-align=center, width=35%]

Com o instalador baixado, dê um duplo clique no mesmo e o processo de
instalação será iniciado. A primeira tela é uma tela de login, entre com os
seus dados footnote:[Para criar a conta é só https://login.qt.io/login[clicar aqui]]
e clique em next (<<img-setup1>>).

[[img-setup1]]
.Primeiro passo da instalação
image::setup_1.png[align=center, title-align=center, width=70%]

Agora você deve marcar as duas opções inferiores (<<img-setup2>>) e clicar em
_Next_ duas vezes.

[[img-setup2]]
.Segundo passo da instalação
image::setup_2.png[align=center, title-align=center, width=70%]

Nó próximo passo pode marcar qualquer uma das opções (<<img-setup3>>), elas
falam sobre o Qt coletar alguns dados anônimos para melhorar o desenvolvimento
do software.

[[img-setup3]]
.Terceiro passo da instalação
image::setup_3.png[align=center, title-align=center, width=70%]

Agora é possível escolher onde o Qt será instalado (<<img-setup4>>), não é
necessário mexer em nada, só clique em _Next_.

[[img-setup4]]
.Quarto passo da instalação
image::setup_4.png[align=center, title-align=center, width=70%]

Agora é necessário escolher qual a versão do Qt será instalada
(<<img-setup5>>), para saber a versão certa sempre consulte algum membro da
equipe, o ideal é que todos usem a mesma versão para evitar problemas de
compatibilidade.

[[img-setup5]]
.Quinto passo da instalação
image::setup_5.png[align=center, title-align=center, width=70%]

Caso você queira minimizar o espaço ocupado pela instalação, é possível desabilitar
algumas funcionalidades que nós não usamos no projeto (<<img-setup5a>>).

[[img-setup5a]]
.Funcionalidades não utilizadas
image::setup_5a.png[align=center, title-align=center, width=50%]

É importante lembrar que, caso necessário, essas funcionalidades podem ser
instaladas posteriormente.

Após isso é só clicar em _Next_ para tudo e o Qt deve ser instalado
corretamente.

=== Testando a instalação

Após instalar o Qt é possível verificar se tudo ocorreu corretamente ao rodar
um exemplo do próprio software. Para isso:

. Abra o Qt.
. Vá para a aba _Welcome_ e selecione os exemplos (<<img-teste1>>).
. Agora escolha um exemplo (caso alguma funcionalidade tenha sido desabilitada
  na instalação pode ser que nem todos os exemplos funcionem, eu recomendo que
  sejam testados exemplos em C++ puro, ou seja, os que não possuem *_(QML)_* no
  nome). Neste tutorial será utilizado o _Analog Clock Window Example_.
. Ao selecionar o exemplo desejado será aberta uma janela explicando como ele
  funciona e tudo mais. Após fechar essa janela cliquem em _Configure Project_
  (<<img-teste2>>).
. Agora o exemplo será aberto como um projeto e você poderá visualizá-lo no Qt.
. Caso você não tenha certeza se seu exemplo foi feito em C++ puro, basta abrir
  as pastas do projeto, só podem existir arquivos _.h, .cpp_ e _.pro_
  (<<img-teste3>>).
. E pronto, agora é só clicar no _Run_ e o exemplo deverá funcionar
  (<<img-teste4>>).

[[img-teste1]]
.Abrindo os exemplos
image::teste_1.png[align=center, title-align=center, width=35%]

[[img-teste2]]
.Configurando o exemplo
image::teste_2.png[align=center, title-align=center, width=70%]

[[img-teste3]]
.Arquivos do exemplo
image::teste_3.png[align=center, title-align=center, width=40%]

[[img-teste4]]
.Rodando o exemplo
image::teste_4.png[align=center, title-align=center, width=40%]

== Criando um projeto

Agora que o Qt já está instalado e funcionando vamos criar o primeiro projeto.
Isso pode ser feito voltando até a aba _Welcome_ -> _Projects_
(<<img-projeto1>>), ou, indo em _File_ -> _New File or Project..._
(kbd:[Ctrl+N])

[[img-projeto1]]
.Criando um projeto novo
image::projeto_1.png[align=center, title-align=center, width=45%]

Agora deverá aparecer uma janela como essa (<<img-projeto2>>). Nela é possível
escolher entre os diversos tipos possíveis de projeto que o Qt pode fazer. Aqui
no RoboFEI nós utilizamos somente os projetos do tipo _Application (Qt)_, que
são os que utilizam somente C/C++.

[[img-projeto2]]
.Tipos de projeto
image::projeto_2.png[align=center, title-align=center, width=60%]

Em relação aos tipos de aplicação (_Qt Widgets_ ou _Qt Console_) a diferença é
que no _Qt Widgets_ será criada uma aplicação com interface gráfica, já no _Qt
Console_ será criada uma aplicação somente com o console (telinha preta).
Durante o treinamento serão feitas aplicações dos dois tipos.

Para este tutorial vamos seguir com o _Qt Widgets_, basta selecioná-lo e clicar
em _Choose_.

Escolha um nome para o projeto e onde ele deve ser salvo (<<img-projeto3>>).
Após escolher, clique em _Next_ *duas vezes*.

[[img-projeto3]]
.Salvando o projeto
image::projeto_3.png[align=center, title-align=center, width=60%]

Agora você pode escolher o nome do "código" (classe) principal
(<<img-projeto4>>), se quiser não precisa mudar nada. Para continuar clique em
_Next_ duas vezes.

[[img-projeto4]]
.Nome da classe principal
image::projeto_4.png[align=center, title-align=center, width=60%]

Na próxima janela não precisa mexer em nada (<<img-projeto5>>), mas, note que
que no mínimo um _kit_ (compilador) deve estar selecionado (o nome do _kit_
pode mudar dependendo do sistema operacional [Windows/Linux]). Clique em _Next_
para continuar.

[[img-projeto5]]
.Kit de compilação do projeto
image::projeto_5.png[align=center, title-align=center, width=60%]

Por fim, será apresentado um resumo com as suas escolhas (<<img-projeto6>>),
clique em _Finish_ para finalizar o processo.

[[img-projeto6]]
.Fim do processo de criação do projeto
image::projeto_6.png[align=center, title-align=center, width=60%]

Ao clicar em _Finish_ o Qt irá abrir o seu projeto e você irá visualizar o
arquivo principal (_main.cpp_), se quiser, pode clicar em _Run_ que uma
janelinha vazia irá aparecer (<<img-projeto7>>).

[[img-projeto7]]
.Rodando o projeto vazio
image::projeto_7.png[align=center, title-align=center, width=70%]

A seta vermelha mostra a árvore do projeto, onde você irá encontrar todos os
arquivos utilizados. A seta verde mostra o arquivo atualmente aberto
(_main.cpp_). A seta azul mostra a janela do projeto.

== Funcionalidades do Qt

Na <<img-teste3>> é possível ver que existem outros 2 botões além do _Run_,
existe o _Debug_ e o _Build_.

=== _Debug_

O botão de _Debug_ possibilita que nós façamos a depuração do código (também
chamado de "debugar" o código), neste modo, é possível inserir _Breakpoints_
nas linhas, isto é, pontos de parada para que: assim que o código for executar
determinada linha, ele pause a execução e, assim, nós podemos visualizar o
conteúdo das variáveis. Isso é muito importante para encontrar e resolver
_bugs_ no código.

Vamos testar isso na prática, vá para o arquivo _main.cpp_ e dê *um* clique no
lado esquerdo do número de alguma linha da função _main()_, um bolinha vermelha
deverá aparecer no lugar, essa bolinha é o _breakpoint_
(<<img-funcionalidade1>>).

[[img-funcionalidade1]]
.Inserindo o primeiro _breakpoint_
image::funcionalidade_1.png[align=center, title-align=center, width=60%]

Agora rode o código no modo _Debug_ (mesmo ícone do _Run_ mas com um inseto).

Provavelmente aquela janela vazia não irá aparecer, mas algumas coisas mudaram
no Qt. Se você observar bem, a bolinha vermelha do _breakpoint_ agora tem uma
seta, indicando que é aquela linha que será executada a seguir
(<<img-funcionalidade2>>).

[[img-funcionalidade2]]
.Depurando o código
image::funcionalidade_2.png[align=center, title-align=center, width=80%]

Vamos para a descrição das setas:

1. Indica qual o _breakpoint_ ativo;
2. Mostra a pilha do programa (lista das chamadas de funções, também conhecida
   como _Call Stack_), como a _main()_ é a primeira função a ser chamada, só
   existe ela na pilha;
3. Mostra as variáveis do "código" (escopo) atual, aqui é possível ver o valor
   dentro de cada variável;
4. Aqui é possível ver a lista com todos os _breakpoints_ que foram colocados
   no código;
5. Nessa parte mostra qual a linha de cada _breakpoint_.

Para prosseguir com a execução do código basta apertar kbd:[F5] ou clicar no
botão de continuar (<<img-funcionalidade3>>).

[[img-funcionalidade3]]
.Botão de continuar execução
image::funcionalidade_3.png[align=center, title-align=center, width=60%]

