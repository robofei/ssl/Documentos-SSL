// Código principal (main)
#include "robot.h"

int main(void)
{
    // Vamos criar (instanciar) um objeto da classe Robot
    Robot r = Robot(); // Aqui é chamado o construtor

    r.vSetID(5);
    r.vSetPosition(QPoint(1500, 2000));

    r.vMove(QVector3D(1.5, 1, 0));
    r.vKick(3);
    return 0;
}

// Ao finalizar a função main() o destrutor será chamado para deletar o objeto r
//
// Também será comum encontra isso utilizando ponteiros, mas esse é um assunto
// para depois

int main_2(void)
{
    // Vamos criar (instanciar) um objeto da classe Robot
    Robot *r = nullptr;
    r = new Robot(); // Aqui é chamado o construtor
    // ou r = new Robot;

    r->vSetID(5);
    r->vSetPosition(QPoint(1500, 2000));

    r->vMove(QVector3D(1.5, 1, 0));
    r->vKick(3);

    delete r; // PARA TODO NEW, DEVE EXISTIR UM DELETE
    return 0;
}
