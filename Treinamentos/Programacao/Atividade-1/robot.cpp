#include "robot.h"
Robot::Robot()
{
    this->id = 0;
    this->detected = false;
    this->angle = 0;
    this->kickStrength = 0;
}

Robot::~Robot()
{
    this->detected = false;
}

// Getters
bool Robot::bDetected()
{
    return this->detected;
}

quint8 Robot::iGetID()
{
    return this->id;
}

QPoint Robot::ptGetPosition()
{
    return this->position;
}

float Robot::fGetAngle()
{
    return this->angle;
}

QVector3D Robot::vt3dGetVelocity()
{
    return this->vel;
}

QPoint Robot::ptGetTargetPosition()
{
    return this->targetPosition;
}

QPoint Robot::ptGetTargetAim()
{
    return this->targetAim;
}


// Setters
void Robot::vSetDetected(bool detected)
{
    this->detected = detected;
}

void Robot::vSetID(quint8 id)
{
    if(id >= 0 && id <= 15)
    {
        this->id = id;
    }
}

void Robot::vSetPosition(QPoint pos)
{
    this->position = pos;
}

void Robot::vSetAngle(float angle)
{
    this->angle = angle;
}

void Robot::vSetVelocity(QVector3D vel)
{
    this->vel = vel;
}

void Robot::vSetTargetPosition(QPoint tPos)
{
    this->targetPosition = tPos;
}

void Robot::vSetTargetAim(QPoint tAim)
{
    this->targetAim = tAim;
}

// Métodos
void Robot::vMove(QVector3D vel)
{
    this->commandVelocity = vel;
}

void Robot::vKick(quint8 kickStrength)
{
    this->kickStrength = kickStrength;
}
