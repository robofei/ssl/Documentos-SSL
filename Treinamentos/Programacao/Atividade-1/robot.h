class Robot
{
public:
    Robot();
    ~Robot();

    // Getters
    bool bDetected();
    quint8 iGetID();
    QPoint ptGetPosition();
    float fGetAngle();
    QVector3D vt3dGetVelocity();
    QPoint ptGetTargetPosition();
    QPoint ptGetTargetAim();

    // Setters
    void vSetDetected(bool detected);
    void vSetID(quint8 id);
    void vSetPosition(QPoint pos);
    void vSetAngle(float angle);
    void vSetVelocity(QVector3D vel);
    void vSetTargetPosition(QPoint tPos);
    void vSetTargetAim(QPoint tAim);

    // Métodos
    void vMove(QVector3D vel);
    void vKick(quint8 kickStrength);

private:
    quint8 id;
    bool detected;

    QPoint position;
    float angle;
    QVector3D vel;
    quint8 kickStrength;

    QPoint targetPosition;
    QPoint targetAim;
    QVector3D targetVelocity;
}
