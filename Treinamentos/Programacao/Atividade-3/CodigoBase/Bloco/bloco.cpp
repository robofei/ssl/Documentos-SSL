#include "bloco.h"

Block::Block()
{
}

const QVector<QPoint> Block::getBlockShape() const
{
    return qAsConst(shape);
}

BLOCK_TYPE Block::type() const
{
    return id;
}

QColor Block::color() const
{
    return myColor;
}

QSharedPointer<Block> Block::createRandomBlock()
{
    BLOCK_TYPE tipoBlock =
        static_cast<BLOCK_TYPE>(QRandomGenerator::global()->bounded(N_BLOCOS));
    QSharedPointer<Block> bloco;

    switch (tipoBlock)
    {
    case BLOCO_I:
        bloco.reset(new BlockI);
        break;
    case BLOCO_J:
        bloco.reset(new BlockJ);
        break;
    case BLOCO_L:
        bloco.reset(new BlockL);
        break;
    case BLOCO_O:
        bloco.reset(new BlockO);
        break;
    case BLOCO_S:
        bloco.reset(new BlockS);
        break;
    case BLOCO_T:
        bloco.reset(new BlockT);
        break;
    case BLOCO_Z:
        bloco.reset(new BlockZ);
        break;
    default:
        qDebug() << "ID de bloco inválido\n";
        bloco.reset(new BlockI);
        break;
    }
    return bloco;
}

/*  ____  _     ___   ____ ___    ___
 * | __ )| |   / _ \ / ___/ _ \  |_ _|
 * |  _ \| |  | | | | |  | | | |  | |
 * | |_) | |__| |_| | |__| |_| |  | |
 * |____/|_____\___/ \____\___/  |___|
 */

BlockI::BlockI()
{
    id = BLOCO_I;
}

BlockI::~BlockI()
{
}

void BlockI::rotateH()
{
}

void BlockI::rotateAH()
{
}

/*  ____  _     ___   ____ ___        _
 * | __ )| |   / _ \ / ___/ _ \      | |
 * |  _ \| |  | | | | |  | | | |  _  | |
 * | |_) | |__| |_| | |__| |_| | | |_| |
 * |____/|_____\___/ \____\___/   \___/
 */

BlockJ::BlockJ()
{
    id = BLOCO_J;
}

BlockJ::~BlockJ()
{
}

void BlockJ::rotateH()
{
}

void BlockJ::rotateAH()
{
}

/*  ____  _     ___   ____ ___    _
 * | __ )| |   / _ \ / ___/ _ \  | |
 * |  _ \| |  | | | | |  | | | | | |
 * | |_) | |__| |_| | |__| |_| | | |___
 * |____/|_____\___/ \____\___/  |_____|
 */

BlockL::BlockL()
{
    id = BLOCO_L;
}

BlockL::~BlockL()
{
}

void BlockL::rotateH()
{
}

void BlockL::rotateAH()
{
}

/*  ____  _     ___   ____ ___     ___
 * | __ )| |   / _ \ / ___/ _ \   / _ \
 * |  _ \| |  | | | | |  | | | | | | | |
 * | |_) | |__| |_| | |__| |_| | | |_| |
 * |____/|_____\___/ \____\___/   \___/
 */

BlockO::BlockO()
{
    id = BLOCO_O;
}

BlockO::~BlockO()
{
}

void BlockO::rotateH()
{
}

void BlockO::rotateAH()
{
}

/*  ____  _     ___   ____ ___    ____
 * | __ )| |   / _ \ / ___/ _ \  / ___|
 * |  _ \| |  | | | | |  | | | | \___ \
 * | |_) | |__| |_| | |__| |_| |  ___) |
 * |____/|_____\___/ \____\___/  |____/
 */

BlockS::BlockS()
{
    id = BLOCO_S;
}

BlockS::~BlockS()
{
}

void BlockS::rotateH()
{
}

void BlockS::rotateAH()
{
}

/*  ____  _     ___   ____ ___    _____
 * | __ )| |   / _ \ / ___/ _ \  |_   _|
 * |  _ \| |  | | | | |  | | | |   | |
 * | |_) | |__| |_| | |__| |_| |   | |
 * |____/|_____\___/ \____\___/    |_|
 */

BlockT::BlockT()
{
    id = BLOCO_T;
}

BlockT::~BlockT()
{
}

void BlockT::rotateH()
{
}

void BlockT::rotateAH()
{
}

/*  ____  _     ___   ____ ___    _____
 * | __ )| |   / _ \ / ___/ _ \  |__  /
 * |  _ \| |  | | | | |  | | | |   / /
 * | |_) | |__| |_| | |__| |_| |  / /_
 * |____/|_____\___/ \____\___/  /____|
 */

BlockZ::BlockZ()
{
    id = BLOCO_Z;
}

BlockZ::~BlockZ()
{
}

void BlockZ::rotateH()
{
}

void BlockZ::rotateAH()
{
}
