#ifndef BLOCO_H
#define BLOCO_H

#include <QColor>
#include <QDebug>
#include <QPoint>
#include <QRandomGenerator>
#include <QSharedPointer>
#include <QVector>
#include <QtMath>

#define BLOCK_SIZE 4

/**
 * @brief Tipos possíveis de blocos
 */
typedef enum BLOCK_TYPE
{
    BLOCO_I = 0,
    BLOCO_J,
    BLOCO_L,
    BLOCO_O,
    BLOCO_S,
    BLOCO_T,
    BLOCO_Z,
    N_BLOCOS // Indica a quantidade máxima de blocos (7)
} BLOCK_TYPE;

class Block
{
protected:
    BLOCK_TYPE id;
    QVector<QPoint> shape;
    QColor myColor;

public:
    Block();
    virtual ~Block(){};

    /**
     * @brief Rotaciona o bloco no sentido horário.
     */
    virtual void rotateH() = 0;

    /**
     * @brief Rotaciona o bloco no sentido anti-horário.
     */
    virtual void rotateAH() = 0;

    /**
     * @brief Retorna a forma do bloco.
     *
     * @return Vetor com a posição de cada quadrado do bloco.
     */
    const QVector<QPoint> getBlockShape() const;

    BLOCK_TYPE type() const;

    QColor color() const;

    void translate(const QPoint& _to);

    bool checkCollision(const QPoint& _test) const;

    bool isInside(const int _limit) const;

    QPoint lowest() const;

    QPoint highest() const;

    /**
     * @brief Gera um bloco aleatório.
     *
     * @return
     */
    static QSharedPointer<Block> createRandomBlock();
};

/*  ____  _     ___   ____ ___    ___
 * | __ )| |   / _ \ / ___/ _ \  |_ _|
 * |  _ \| |  | | | | |  | | | |  | |
 * | |_) | |__| |_| | |__| |_| |  | |
 * |____/|_____\___/ \____\___/  |___|
 */

class BlockI : public Block
{

public:
    BlockI();
    ~BlockI();

    void rotateH();
    void rotateAH();
};

/*  ____  _     ___   ____ ___        _
 * | __ )| |   / _ \ / ___/ _ \      | |
 * |  _ \| |  | | | | |  | | | |  _  | |
 * | |_) | |__| |_| | |__| |_| | | |_| |
 * |____/|_____\___/ \____\___/   \___/
 */

class BlockJ : public Block
{

public:
    BlockJ();
    ~BlockJ();

    void rotateH();
    void rotateAH();
};

/*  ____  _     ___   ____ ___    _
 * | __ )| |   / _ \ / ___/ _ \  | |
 * |  _ \| |  | | | | |  | | | | | |
 * | |_) | |__| |_| | |__| |_| | | |___
 * |____/|_____\___/ \____\___/  |_____|
 */

class BlockL : public Block
{

public:
    BlockL();
    ~BlockL();

    void rotateH();
    void rotateAH();
};

/*  ____  _     ___   ____ ___     ___
 * | __ )| |   / _ \ / ___/ _ \   / _ \
 * |  _ \| |  | | | | |  | | | | | | | |
 * | |_) | |__| |_| | |__| |_| | | |_| |
 * |____/|_____\___/ \____\___/   \___/
 */

class BlockO : public Block
{

public:
    BlockO();
    ~BlockO();

    void rotateH();
    void rotateAH();
};

/*  ____  _     ___   ____ ___    ____
 * | __ )| |   / _ \ / ___/ _ \  / ___|
 * |  _ \| |  | | | | |  | | | | \___ \
 * | |_) | |__| |_| | |__| |_| |  ___) |
 * |____/|_____\___/ \____\___/  |____/
 */

class BlockS : public Block
{

public:
    BlockS();
    ~BlockS();

    void rotateH();
    void rotateAH();
};

/*  ____  _     ___   ____ ___    _____
 * | __ )| |   / _ \ / ___/ _ \  |_   _|
 * |  _ \| |  | | | | |  | | | |   | |
 * | |_) | |__| |_| | |__| |_| |   | |
 * |____/|_____\___/ \____\___/    |_|
 */

class BlockT : public Block
{

public:
    BlockT();
    ~BlockT();

    void rotateH();
    void rotateAH();
};

/*  ____  _     ___   ____ ___    _____
 * | __ )| |   / _ \ / ___/ _ \  |__  /
 * |  _ \| |  | | | | |  | | | |   / /
 * | |_) | |__| |_| | |__| |_| |  / /_
 * |____/|_____\___/ \____\___/  /____|
 */

class BlockZ : public Block
{

public:
    BlockZ();
    ~BlockZ();

    void rotateH();
    void rotateAH();
};

#endif // BLOCO_H
