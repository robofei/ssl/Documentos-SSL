#ifndef TETRIS_H
#define TETRIS_H

#include "Bloco/bloco.h"
#include <QObject>
#include <QVector>

#define TETRIS_WIDTH 10
#define TETRIS_HEIGHT 20
#define SPAWN_POINT QPoint(TETRIS_WIDTH / 2, TETRIS_HEIGHT - 1)

class Tetris : public QObject
{
    Q_OBJECT

public:
    Tetris();
};

#endif // TETRIS_H
