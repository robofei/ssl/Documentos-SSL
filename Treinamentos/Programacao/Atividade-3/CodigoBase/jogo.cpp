#include "jogo.h"

#include "Bloco/bloco.h"
#include "ui_jogo.h"
#include <cstdlib>

Jogo::Jogo(QWidget* parent) : QMainWindow(parent), ui(new Ui::Jogo)
{
    ui->setupUi(this);

    clock.reset(new QTimer);
    connect(clock.get(), &QTimer::timeout, this, &Jogo::clockTick);
    clock.get()->start(TEMPO_CLOCK);

    labelOffset = 8;

    // Exemplo para gerar um bloco aleatório
    QSharedPointer<Block> currentBlock = Block::createRandomBlock();
}

Jogo::~Jogo()
{
    delete ui;
}

void Jogo::drawBlock(int x, int y, QColor cor, QColor inside)
{
    QPainter pincel;

    pincel.begin(&screen);
    pincel.translate(
        QPointF(labelOffset / 4.0, screen.height() - labelOffset / 4.0));
    pincel.scale(scaleX, scaleY);

    pincel.setBrush(inside);
    pincel.setPen(QPen(cor, 0.1));

    pincel.drawRect(QRectF(x, -(y + 2 / scaleY), 1, -1));

    pincel.end();
}

void Jogo::draw()
{
    setup();

    drawBlock(0, 0, Qt::darkGray, Qt::red);
    drawBlock(TETRIS_WIDTH-1, 0, Qt::darkGray, Qt::red);
    drawBlock(0, TETRIS_HEIGHT-1, Qt::darkGray, Qt::red);
    drawBlock(TETRIS_WIDTH-1, TETRIS_HEIGHT-1, Qt::darkGray, Qt::red);

    ui->labelTela->setPixmap(screen);
}

void Jogo::setup()
{
    if (ui->labelTela->size() != screen.size())
    {
        screen = QPixmap(ui->labelTela->size());
    }
    screen.fill(Qt::black);
    scaleX = 1.f * (screen.size().width() - labelOffset) / TETRIS_WIDTH;
    scaleY = 1.f * (screen.size().height() - labelOffset) / TETRIS_HEIGHT;
}

void Jogo::clockTick()
{
    draw();
}
