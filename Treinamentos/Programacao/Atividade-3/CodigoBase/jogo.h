#ifndef JOGO_H
#define JOGO_H

#include "Tetris/tetris.h"
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui
{
class Jogo;
}
QT_END_NAMESPACE

#define TEMPO_CLOCK 500

class Jogo : public QMainWindow
{
    Q_OBJECT

public:
    Jogo(QWidget* parent = nullptr);
    ~Jogo();

private:
    Ui::Jogo* ui;
    QSharedPointer<QTimer> clock;
    QPixmap screen;
    float scaleX;
    float scaleY;
    int labelOffset;

    Tetris tetrisGame;

    void drawBlock(int x, int y, QColor cor, QColor inside);
    void draw();
    void setup();

private slots:

    /**
     * @brief Base de tempo para o jogo
     */
    void clockTick();
};
#endif // JOGO_H
