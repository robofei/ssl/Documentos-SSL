= Atividade 3 - Tetris
:imagesdir: ./Imagens/
:source-highlighter: rouge
include::attributes-pt.adoc[]
:xrefstyle: short
:docinfo1:
:sectnums:
:toc:

== Introdução

Na terceira e última atividade iremos recriar o clássico jogo Tetris
(<<img-tetris>>).

[[img-tetris]]
.Tetris
image::tetris.png[align=center, title-align=center, width=35%]

No jogo, existem 7 tipos de blocos: I, J, L, O, S, T e Z (<<img-blocos>>).

[[img-blocos]]
.Tipos de Blocos
image::blocos.png[align=center, title-align=center, width=60%]

Os blocos são gerados aleatoriamente no topo da tela e a velocidade de descida
aumenta conforme o tempo passa. Todos os blocos podem ser rotacionados de 90 em
90 graus.

Ao completar uma fileira contínua, a mesma é eliminada da tela, liberando
espaço para o jogador. Caso os blocos cheguem ao topo da tela, o jogador perde.

== Objetivo

O objetivo desta atividade é implementar o jogo Tetris utilizando os conceitos
de programação orientada à objetos e também as funcionalidades do Qt. Um código
base para implementação do jogo será fornecido para ajudar no início.

== Implementação

A partir dos conceitos de orientação à objetos deverão ser utilizadas (no
mínimo) três classes (*Bloco*, *Tela* e *Jogo*) para a implementação da lógica
do jogo. Mais classes podem ser utilizadas para lidar com o desenho das peças
na tela e etc.

=== Classe Bloco

Como o próprio nome diz, a classe bloco será utilizada para implementar os
blocos do jogo e seu comportamento, como, rotação e deslocamento (vertical e
horizontal).

A partir do conceito de *herança* é possível fazer com que as outras partes do
programa sejam capazes de lidar com os blocos de forma genérica, sem precisar
saber com qual tipo de bloco estamos lidando. O código base para isso está
disponível na pasta *Tetris*.

=== Classe Tela

A classe tela será utilizada para implementar as funções da "tela" do jogo,
isto é, ela que irá fazer o controle dos blocos que já foram colocados
(estacionários), se existe alguma linha completa para ser eliminada, como ela
será eliminada e etc.

=== Classe Jogo

A classe jogo é a classe principal do projeto, ela será responsável por receber
as entradas do jogador, que serão:

* Teclas
** Esquerda (ou A): Movimenta o bloco para a esquerda
** Baixo (ou S): Acelera a descida do bloco
** Direita (ou D): Movimenta o bloco para a direita
** Q: Gira o bloco para a esquerda
** E: Gira o bloco para a direita

A classe jogo também pode lidar com o desenho do jogo na interface, mas seria
melhor utilizar uma outra classe para isso.

=== Sobre o jogo

No código base já está implementado um *timer*, este *timer* tem o objetivo de
gerar uma base de tempo para fazer a atualização do jogo, isto é, a cada
*tick*/sinal do *timer* o jogo deve analisar o bloco que está descendo, os
blocos que já foram colocados, movimentar o bloco, desenhar o jogo na interface
e etc.

O slot do timer se chama *clockTick()* e é chamado a cada *TEMPO_CLOCK*
milissegundos.

=== Desenhando

Para fazer o desenho do jogo utilize como referência este
https://doc.qt.io/qt-5/qpainter.html[link].

Em resumo, será necessário criar um *label* na interface e então fazer algo
assim para fazer os desenhos:

[source, cpp, linenums, highlight]
----
// Os argumentos são o tamanho (x, y) da imagem, neste caso, 200 x 1000
QPixmap desenhoJogo(200, 1000);
QPainter pincel;

pincel.begin(&desenhoJogo);
// Desenha uma linha do ponto (0,0) até (100, 500)
pincel.drawLine(0,0, 100, 500);
pincel.end();


ui->label->setPixmap(&desenhoJogo);
----

== Dicas

Uma ordem a seguir para a implementação do jogo pode ser:

. Representação dos blocos
. Receber as ações do jogador (teclas do teclado)
. Rotação dos blocos
. Movimentação dos blocos
. Representação dos blocos na tela (estacionários)
. Detecção da colisão dos blocos
. Eliminação de linhas completas

O jogo possui um tamanho de 10 x 20 (x, y), da seguinte forma:

[[img-tetrix-ex]]
.Exemplo Tetris
image::tetris_m.svg[align=center, title-align=center, width=35%]

Uma forma de gerar os blocos aleatoriamente também já é fornecida no código
base.

== Apêndice

Aqui serão explicados alguns conceitos envolvidos nesta atividade.

=== Herança

O primeiro conceito é o de *herança*. Este é um conceito presente em todas
linguagens orientadas à objeto (Java, C++, Python ...) e é muito útil para
padronizar o código e também reaproveitar alguma implementação. No caso desta
atividade este conceito é utilizado para padronizar a interação com os blocos.

A sintaxe para criar uma herança é bem simples, dadas as classes *A* e *B*,
caso *B* herde *A* temos:

[source, cpp, linenums, highlight]
----
class A
{
public:
    int algumAtributo;
    void fazAlgumaCoisa(int a, int b);
};

class B : public A
{
public:
    void fazOutraCoisa(void);
};
----

O que acontece numa herança é que a classe filha (também chamada de subclasse)
herda todos os atributos e métodos da classe pai (também chamada de
superclasse), ou seja, é possível fazer o seguinte com a classe  *B*:

[source, cpp, linenums, highlight]
----
int main(void)
{
    B objB;
    objB.fazAlgumaCoisa(1, 2); // Método da classe A
    objB.algumAtributo = 10; // Atributo da classe A

    objB.fazOutraCoisa(); // Método da classe B
    return 0;
}
----

Dessa forma, quando fazemos a separação do código em diversas classes, as
classes que possuem comportamentos similares podem ter os método/atributos em
comum implementados em uma superclasse, e os comportamentos distintos
implementados em outras subclasses.

==== Tipos de acesso e o modificador _protected_

Por padrão, a herança é realizada de forma privada (_private_):

[source, cpp, linenums, highlight]
----
class A
{
public:
    int algumAtributo;
    void fazAlgumaCoisa(int a, int b);
};

class B : A
{
    ...
};

int main()
{
    B objB;
    // objB.fazAlgumaCoisa(1, 1); // Gera um erro de compilação, pois o método foi herdado de forma privada
    return 0;
}
----

Neste caso, os métodos/atributos que são públicos na superclasse serão herdados
como privados na subclasse. É por conta disso que na maioria do casos a herança
é feita adicionando o modificador _public_:

[source, cpp, linenums, highlight]
----
class B : public A
{
    ...
};
----

Mas e quando queremos que um método/atributo seja privado na superclasse *e* na
subclasse? É aí que utilizamos o modificador _protected_:

[source, cpp, linenums, highlight]
----
class A
{
protected:
    int algumAtributo;
    void fazAlgumaCoisa(int a, int b);
};

class B : public A
{
public:
    void fazOutraCoisa(void);
};
----

Neste caso, os métodos/atributos da classe *A* são privados e ao herdá-los na
classe *B* eles continuam sendo privados:

[source, cpp, linenums, highlight]
----
int main(void)
{
    A objA;
    B objB;
    // objA.fazAlgumaCoisa(1, 2); // Gera um erro de compilação, pois o método é privado
    // objB.fazAlgumaCoisa(3, 4); // Gera um erro de compilação, pois o método é privado
    // objB.algumAtributo = 10; // Gera um erro de compilação, pois o atributo é privado

    objB.fazOutraCoisa();
    return 0;
}
----

=== Polimorfismo

O polimorfismo é um outro conceito importantíssimo para tornar o código modular
e genérico. Ele permite que uma classe mais abstrata (superclasse) represente o
comportamento das classes especializadas (subclasse). Trazendo isso para a
nossa atividade, o polimorfismo torna possível fazer com que, embora existam 7
tipos de blocos, todos sejam manipulados/utilizados a partir da superclasse dos
blocos. Isto é utilizado no método para gerar um bloco aleatório (fornecido no
código base):

[source, cpp, linenums, highlight]
----
QSharedPointer<Bloco> Bloco::GerarBloco()
{
    TiposBlocos tipoBloco = static_cast<TiposBlocos>(QRandomGenerator::global()->bounded(N_BLOCOS));
    QSharedPointer<Bloco> bloco;

    switch (tipoBloco)
    {
        case BLOCO_I:
            bloco.reset(new BlocoI);
            break;
        case BLOCO_J:
            bloco.reset(new BlocoJ);
            break;
        case BLOCO_L:
            bloco.reset(new BlocoL);
            break;
        case BLOCO_O:
            bloco.reset(new BlocoO);
            break;
        case BLOCO_S:
            bloco.reset(new BlocoS);
            break;
        case BLOCO_T:
            bloco.reset(new BlocoT);
            break;
        case BLOCO_Z:
            bloco.reset(new BlocoZ);
            break;
        default:
            qDebug() << "ID de bloco inválido\n";
            bloco.reset(new BlocoI);
            break;
    }
    return bloco;
}
----

Este método cria um novo bloco aleatoriamente e retorna uma referência para a
classe *Bloco* (genérica). Dessa forma, não é necessário que o código que está
utilizando esta função conheça qual o tipo de bloco está sendo criado, isto não
importa uma vez que o próprio bloco deve conter os métodos necessários para
manipulá-lo (rotacionar, descer e etc). Isso faz com que seja simples de
adicionar outros tipos de bloco no futuro, por exemplo.

Em outras palavras, a classe *Bloco* é uma *interface*, ou seja, ela
proporciona um template genérico para interação com as subclasses, de forma
que, o comportamento em si é implementado pelas subclasses.

==== A necessidade dos ponteiros

Diferente do Java, que foi feito para ser 100% orientado à objetos, logo, TUDO
é uma classe/referência, no C++ é explicito ao programador o uso de ponteiros
para realizar algumas implementações, especialmente quando for utilizar
polimorfismo.

Para entender o que é um ponteiro, vamos imaginar que foi criada uma variável
*abc* que guarda uma string de caracteres, essa variável vai ser armazenada em
um pedacinho da memória RAM do computador, como na <<img-ponteiro>>:

[[img-ponteiro]]
.Ponteiro
image::ponteiro.svg[align=center, title-align=center, width=35%]

NOTE: Todas strings possuem um caractere nulo (zero) no final para indicar que
a string acabou.

Neste pedaço da memória, no endereço *1* está armazenada a letra *A*, no
endereço *2* está armazenada a letra *B* e daí por diante. O ponteiro é um tipo
de variável que é capaz de armazenar uma referência para um endereço de
memória, em outras palavras, o que o ponteiro armazena são as posição de
memória (1, 2, 3, 4, 5, 6, 7 ou 8).

Agora vamos usar um ponteiro para brincar com essa string, seria algo desse
tipo:

[source, cpp, linenums, highlight]
----
#include <iostream>
int main()
{
    char abc[] = "ABCDEFG";

    /*
     * Um ponteiro comum é indicado pelo asterisco (*).
     *
     * Nesta declaração está sendo criado um ponteiro do tipo char e o endereço
     * que este ponteiro irá apontar é o endereço da variável abc.
    */
    char* ponteiro = abc;

    std::cout << ponteiro;

    return 0;
}
----

Se você rodar esse código a string "ABCDEFG" deverá aparecer no seu terminal,
mas o que aconteceu? Basicamente, foi criado um ponteiro e o endereço que ele
aponta foi definido como o endereço inicial da string, dessa forma é possível
acessar os dados da string.

A vantagem do ponteiro é que, como ele é só uma referência para o endereço, ele
ocupa somente o tamanho necessário para representar o endereço da variável (8
bytes), diferente do vetor *abc*, que irá ocupar *8 bytes* (tamanho necessáio
para guardar a string ABCDEFG). Como essa string é pequena, não tem diferença
nenhuma, mas para uma string maior, por exemplo, é possível testar isso da
seguinte forma:

[source, cpp, linenums, highlight]
----
int main()
{
    char abc[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char* ponteiro = abc;
    std::cout << "Tamanho da string: " << sizeof(abc) <<
                 " Tamanho do ponteiro: " << sizeof(ponteiro) << " Bytes"
                 << std::endl;
    return 0;
}
----

A saída será algo do tipo:

[source]
----
Tamanho da string: 27 Tamanho do ponteiro: 8 Bytes
----

Também é possível fazer outras coisas. Ao incrementar o ponteiro, estamos
incrementando o endereço que ele aponta, e assim, caminhamos pela memória, ou
seja, caso fosse necessário mostrar somente a letra *D* bastaria fazer o
seguinte:

[source, cpp, linenums, highlight]
----
int main()
{
    char abc[] = "ABCDEFG";
    char* ponteiro = abc;
    std::cout << *(ponteiro+3);
    return 0;
}
----

Após a declaração do ponteiro, o asterisco serve para acessar o *valor* do endereço
de memória que o ponteiro está apontando, ou seja, o que foi feito na expressão
* *(ponteiro+3)* é:

- (ponteiro+3): O endereço do ponteiro foi incrementado 3 vezes, chegando ao
  endereço 4 (ver <<img-ponteiro>>).
- *(ponteiro+3): Foi acessado o valor do endereço 4 de memória (letra D).

Visualmente temos o seguinte:

[[img-ponteiro-vis]]
.Ponteiro
image::ponteiro_2.svg[align=center, title-align=center, width=35%]

No entanto, o uso de ponteiros de forma crua (_raw pointers_) não é
recomendado, ainda mais para programadores inexperientes, pois como pode ser
visto, é possível fazer com o ponteiro invada áreas inválidas de memória,
gerando _crashes_, _bugs_ complexos e vazamentos de memória (_memory leaks_).
Para facilitar isso existem os _smart pointers_, esses _smart pointers_ tomam
conta sozinhos de todas as ações que podem gerar problemas. Um exemplo deles no
Qt é o *QSharedPointer*, usado no código base da atividade.

Voltando ao uso do ponteiro no polimorfismo, ele é importante pois, em muitos
dos casos, inclusive nesta atividade, o polimorfismo é feito utilizando classes
abstratas, que são classes que não possuem a declaração de seus métodos, só a
definição, forçando o programador a implementar o método na subclasse. Assim,
por não possuir a declaração dos métodos, o objeto desta classe não pode ser
instanciado, só podem ser criadas referências. Você pode verificar isso
tentando instanciar um objeto da classe *Bloco*:

[source, cpp, linenums, highlight]
----
{
    Bloco blocoTeste; // Vai gerar um erro de compilação
    QSharedPointer<Bloco> blocoTeste; // Mas isso não vai
    blocoTeste.reset();
}
----
