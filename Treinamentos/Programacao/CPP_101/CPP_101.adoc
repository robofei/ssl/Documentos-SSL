= C++ 101
:source-highlighter: rouge
include::attributes-pt.adoc[]
:xrefstyle: short
:docinfo1:
:sectnums:
:toc:

== Objetivo

Este documento tem como objetivo fornecer uma noção básica sobre a estrutura de
um código em C/C++, o que são e como funcionam as variáveis e tipos de
variáveis, estruturas condicionais, laçoes de repetição e funções.

== Sintaxe Básica

De forma geral, qualquer programa básico vai possuir uma estrutura parecida
com o que pode ser visto abaixo.

[source, cpp, linenums, highlight]
----
// 1 - Inclusão de bibliotecas
#include <stdio.h> // Standard IO (Funções de entrada/saída) da linguagem C
#include <iostream> // Funções de entrada/saída da linguagem C++

// 2 - Função principal do programa
int main()
{

    return 0;
}
----

A primeira coisa que aparece no arquivo é a inclusão das bibliotecas, todas as
bibliotecas que forem ser utilizadas ao longo do código *devem* ser incluídas
no início do arquivo.

Também podem ser incluídas bibliotecas/arquivos feitos pelo próprio
usuário/programador, isto geralmente é diferenciado pelo uso de *<* *>*, no
primeiro caso, e aspas duplas *"* *"*, no segundo caso.

[source, cpp, linenums, highlight]
----
#include <iostream> // Biblioteca padrão da linguagem
#include "minhaBiblioteca.h" // Biblioteca feita pelo usuário
----

NOTE: https://www.cplusplus.com/reference/cstdio/[Clique para ver as funcionalidades da *stdio.h*]

NOTE: https://www.cplusplus.com/reference/iostream/[Clique para ver as funcionalidades da *iostream*]

A segunda coisa que aparece é a função _*main*_, ela é a função principal do
código e *sempre* deve existir no *projeto*, ou seja, digamos existam 50
arquivos *.cpp* no seu projeto, em algum desses arquivos deve ser feita a
definição da função *main*, caso contrário será gerado um erro de compilação.

[[img-main-undef]]
.Erro quando uma função main não é definida
image::./Imagens/main_undefined.png[align=center, title-align=center, width=100%]

== Variáveis

Para construir qualquer tipo de código sempre será necessário armazenar algum
tipo de valor, o resultado de uma conta, e coisas do tipo, para isso utilizamos
as variáveis. Em C++, cada variável *deve* ter um tipo definido, e cada tipo de
variável é utilizado para armazenar um tipo de dado. Na tabela abaixo é
possível ver os tipos mais comuns e qual tipo de dado eles são capazes de
armazenar.

.Tipos de variáveis e seus usos
[options="header,footer",cols="10, 50, 20"]
|====
| Tipo   | Tipo de dado | Tamanho [bytes]
| int      | Valores inteiros, como: 0, 1, 2, 3 | 2 ou 4
| char     | Caracteres, como: 'a', 'b', 'c', 'd' | 1
| float    | Valores reais, como: 1.203, 2.50202 | 4
| double   | Valores reais com mais casas de precisão | 8
| string   | Texto, como: "Algum texto", "Mais um texto" | tamanho + 1
| bool     | Valores booleanos, como: true ou false | 1
| void     | Nada. Este tipo geralmente é utilizado para declarar funções que não retornam nada | 0
|====

Para declarar uma variável a sintaxe é a seguinte:

[source, cpp, linenums, highlight]
----
// tipoDaVariavel nomeDaVariavel = valorInicial;
// Exemplos:
int umaVariavel = 10;
char outraVariavel = 'A';
float maisUmaVariavel = 1.23;
string umaVariavelComTexto = "Vou armazenar esta frase";
bool umaVariavelBooleana = true;
----

Algumas particularidades dos tipos de variáveis são:

* Ao dividir 2 variáveis do tipo *int* a parte fracionária é perdida
  (truncada), ou seja, para:

[source, cpp, linenums, highlight]
----
int a = 5;
int b = 2;
int c = a/b; // c vai ser igual à 2, pois 5/2 = 2.5, truncando o 0.5 temos 2
----

* O tipo *char* possui apenas 1 byte, ou seja, ele pode armazenar valores de 0
  a 255, essa é a quantidade suficiente para armazenar qualquer código da
  https://u.cs.biu.ac.il/~kalechm/c_04/full_ASCII_table.pdf[tabela ASCII], que
  é como o computador compreende todos os caracteres possíveis (alfabeto
  maiúsculo e minúsculo, numerais e muitos outros)
** Note que: o caractere '0' possui um valor *decimal* de 48, o caractere '1'
   possui um valor *decimal* de 49, de acordo com a
   https://u.cs.biu.ac.il/~kalechm/c_04/full_ASCII_table.pdf[tabela ASCII],
   assim, quando tivermos em mãos números em formato de caractere, para obter o
   valor *numérico* do caractere temos que subtrair *48* ou '0', por exemplo:

[source, cpp, linenums, highlight]
----
char numChar = '9';
int valorChar = numChar - '0';
// ou
// int valorChar = numChar - 48;
// Assim, o contéudo de valorChar será 9
----

== Estruturas Condicionais

A primeira é o *if*, ele serve para comparar se uma certa condição é verdadeira
ou não, caso ela seja, o bloco de código dentro do *if* será executado:

[source, cpp, linenums, highlight]
----
// if(condição é verdadeira)
// {
//     Executa isso
// }
int a = 10;

if(a < 10)
{
    a = 1;
}
// No fim, a é 1
----

O *else* serve para, caso a condição do *if* seja falsa, o que está dentro do
*else* é que será executado:

[source, cpp, linenums, highlight]
----
// if(condição é verdadeira)
// {
//     Executa isso
// }
// else // Senão
// {
//     Executa isso
// }
int a = 10;

if(a < 10)
{
    a = 1;
}
else
{
    a = 2;
}
// No fim, a é 2
----

Também é possível combinar o *if* com o *else* e aí temos o *else if*, ele
serve para caso a condição do *if* anterior seja falsa, podemos checar uma
segunda condição:

[source, cpp, linenums, highlight]
----
// if(condição é verdadeira)
// {
//     Executa isso
// }
// else if(outra condição é verdadeira) // Senão se...
// {
//     Executa isso
// }
int a = 20;

if(a < 10)
{
    a = 1;
}
else if(a > 15)
{
    a = 2;
}
// No fim, a é 2
----

É possível combinar esses comandos para fazer diversas verificações de maneira
sequencial, ou seja:

[source, cpp, linenums, highlight]
----
// if(condição é verdadeira)
// {
//     Executa isso
// }
// else if(outra condição é verdadeira) // Senão se...
// {
//     Executa isso
// }
// else if(outra condição é verdadeira) // Senão se...
// {
//     Executa isso
// }
// else if(outra condição é verdadeira) // Senão se...
// {
//     Executa isso
// }
// else // Senão
// {
//     Executa isso
// }

int a = 11;

if(a < 10)
{
    a = 1;
}
else if(a > 15)
{
    a = 2;
}
else if(a > 12)
{
    a = 3;
}
else
{
    a = 4;
}
// No fim, a é 4
----

É importar ressaltar que: tudo deve *sempre iniciar com um if*, só pode haver
*um único else* e ele deve ser o último bloco, podem haver *inúmeros else if*
no meio.

=== Comparadores e Operações Lógicas

Para realizar as comparações das condições do *if* e *else if* podemos utilizar
diversos comparadores, como:

.Comparadores
[options="header,footer",cols="15,100"]
|====
| Operador | Função
| a < b  | Retorna *true* se *a* for *menor* que *b*, caso contrário, *false*
| a > b  | Retorna *true* se *a* for *maior* que *b*, caso contrário, *false*
| a <= b | Retorna *true* se *a* for *menor ou igual* que *b*, caso contrário, *false*
| a >= b | Retorna *true* se *a* for *maior ou igual* que *b*, caso contrário, *false*
| a == b | Retorna *true* se *a* for *igual* que *b*, caso contrário, *false*
| a != b | Retorna *true* se *a* for *diferente* que *b*, caso contrário, *false*
|====

Além disso, existem operadores lógicos, para combinar diversas condições:

.Operações Lógicas
[options="header,footer",cols="15,100"]
|====
| Operador | Função
| A && B | *AND*: Operação lógica *E*, a condição *A E B* devem ser verdadeiras para a operação retornar *true*
| A \|\| B | *OR*: Operação lógica OU, a condição *A OU B* devem ser verdadeiras para a operação retornar *true*
| !A | *NOT*: Operação lógica de negação, a condição *A* deve ser *false* para a operação retornar *true*
|====

Note que, no caso da operação *AND* (&&), caso a condição *A* seja falsa, a
condição *B* *não será testada*, isso é útil quando queremos verificar alguma condição
utilizando um vetor, mas somente caso ele não seja vazio, por exemplo:

[source, cpp, linenums, highlight]
----
vector<int> vetor;

// Neste exemplo, caso o vetor esteja vazio, isso não causará um erro no
// programa, pois o acesso ao vetor só sera feito quando a condição de
// vetor.size() > 0 for verdadeira
if(vetor.size() > 0 && vetor[0] == 10)
{
    vetor[0] = 20;
}
----

== Laços de Repetição

Existem três tipos de laços de repetição: *for*, *while* e o *do while*.

O laço *for* é o mais clássico e sua sintaxe é a seguinte:

[source, cpp, linenums, highlight]
----
// for(inicialização; condição de parada; forma de incremento)
// {
//     ação a ser repetida
// }
// Exemplo:

// As três formas de fazer o incremento do i são equivalentes, porém:
for(int i = 0; i < 10; i = i + 1) // Serve para qualquer incremento/decremento, +1, -1, +3, -3 ...
for(int i = 0; i < 10; i++) // Serve somente para incremento/decremento unitário, +1 (i++) e -1 (i--)
for(int i = 0; i < 10; i += 1) // Serve para qualquer incremento/decremento, igual a primeira forma
----

No exemplo anterior, a variável de controle do *for* (*i*) foi definida dentro
do próprio comando do *for*, dessa forma, a variável *i* *só pode ser utilizada
dentro do laço*, ou seja, ela é local do laço. Caso seja necessário também é
possível fazer a declaração fora do *for*:

[source, cpp, linenums, highlight]
----
int n;
for(n = 0; n < 10; n++)
{
}
----

Ou seja, o laço do tipo *for* é um laço que automaticamente já
incrementa/decrementa a variável de controle e verifica se a condição final é
verdadeira ou não, caso seja, o laço é finalizado. Os exemplos anteriores
representam laços que executam o bloco interno 10 vezes (n = 0 até n < 10, ou
seja, n = 9).

O laço *while* é um pouco mais simples, ele executa tudo que está no seu bloco
*enquanto* a condição for verdadeira, sua sintaxe é a seguinte:

[source, cpp, linenums, highlight]
----
// while(condição)
// {
//     ação a ser repetida
// }
int n = 0;

while(n < 10)
{
    // Faz alguma coisa
    n++;
}
----

Note que, no laço *while* é obrigatório realizar o incremento/decremento da
variável de controle manualmente, caso contrário o código nunca irá finalizar,
ficará preso dentro do laço para *sempre*. Outra peculiaridade é que, o laço
*while* só será executado caso a condição seja verdadeira logo no início, por
exemplo:

[source, cpp, linenums, highlight]
----
int n = 10;

while(n < 10)
{
    n++;
}
----

Nesse exemplo, o bloco interno do *while* *nunca* será executado, pois a
condição de entrada é que *n* seja menor que 10.

Então, temos o laço *do while*, onde, independente da condição final, o laço
*sempre* será executado *no mínimo uma vez*. Sua sintaxe pode ser vista a
seguir.

[source, cpp, linenums, highlight]
----
// do
// {
//     ação a ser repetida
// }while(condição);

int n = 10;
char b = 'a';

do
{
    b = 'c';
}while(n < 10);
// no fim, b conterá o caractere 'c'
----

Se o exemplo anterior fosse feito com *while*:

[source, cpp, linenums, highlight]
----
int n = 10;
char b = 'a';

while(n < 10)
{
    b = 'c';
}
// no fim, b conterá o caractere 'a'
----

== Funções

As funções são uma forma de organizar melhor o código, separando trechos que
realizam tarefas diferentes em funções que podem até mesmo ser reutilizadas,
por exemplo, digamos que tenhamos de construir um código que calcula a área de
diversas formas geométricas dadas as dimensões fornecidas pelo usuário. Um
programador que não possui o conhecimento de funções poderia fazer algo do
tipo:

[source, cpp, linenums, highlight]
----
#include <iostream>
int main()
{
    int ladoQuadrado = 0, baseTriangulo = 0,
        alturaTriangulo = 0, raioCirculo = 0;
    char opcaoUsuario = 0;

    // Lê opção do usuário
    std::cout << "Digite uma opção de área para calcular\n\tQ - Quadrado\n\tT - Triangulo\n\tC - Circulo\nOpcao = ";
    std::cin >> opcaoUsuario;

    if(opcaoUsuario == 'Q')
    {
        char valor[10];
        std::cout << "Digite o lado do quadrado: ";
        std::cin >> valor;
        ladoQuadrado = std::stoi(valor);

        std::cout << "A área do quadrado é: ";
        std::cout << ladoQuadrado * ladoQuadrado << "\n";
    }
    else if(opcaoUsuario == 'T')
    {
        char valor[10];
        std::cout << "Digite a base do triangulo: ";
        std::cin >> valor;
        baseTriangulo = std::stoi(valor);

        std::cout << "Digite a altura do triangulo: ";
        std::cin >> valor;
        alturaTriangulo = std::stoi(valor);

        std::cout << "A área do triangulo é: ";
        std::cout << baseTriangulo * alturaTriangulo / 2 << "\n";
    }
    else if(opcaoUsuario == 'C')
    {
        char valor[10];
        std::cout << "Digite o raio do circulo: ";
        std::cin >> valor;
        raioCirculo = std::stoi(valor);

        std::cout << "A área do circulo é: ";
        std::cout << 3.1415 * raioCirculo * raioCirculo << "\n";
    }
    else
    {
        std::cout << "\nOpção inválida! Use Q, T ou C\n";
    }
    return 0;
}
----

Porém, existem pedaços do código que se repetem com frequência, além disso, o
cálculo das áreas poderia ser feito por funções independentes, assim, caso
fosse necessário calcular novamente uma área, bastaria chamar a função. Reorganizando
melhor o código temos algo assim:

[source, cpp, linenums, highlight]
----
#include <iostream>

float areaQuadrado(int lado)
{
    return lado * lado;
}

float areaTriangulo(int base, int altura)
{
    // Note que foi utilizado o 2.0 para evitar que o resultado seja truncado
    return base * altura / 2.0;
}

float areaCirculo(int raio)
{
    return 3.1415 * raio * raio;
}

int lerEntrada()
{
    // 10 é o tamanho máximo do valor inserido pelo usuário
    char valor[10];
    std::cin >> valor;
    return std::stoi(valor); // Converte de string para int
}

void calculaAreaQuadrado()
{
    int ladoQuadrado = 0;
    std::cout << "Digite o lado do quadrado: ";
    ladoQuadrado = lerEntrada();

    std::cout << "A área do quadrado é: ";
    std::cout << areaQuadrado(ladoQuadrado) << "\n";
}

void calculaAreaTriangulo()
{
    int baseTriangulo = 0, alturaTriangulo = 0;
    std::cout << "Digite a base do triangulo: ";
    baseTriangulo = lerEntrada();
    std::cout << "Digite a altura do triangulo: ";
    alturaTriangulo = lerEntrada();

    std::cout << "A área do triangulo é: ";
    std::cout << areaTriangulo(baseTriangulo, alturaTriangulo) << "\n";
}

void calculaAreaCirculo()
{
    int raioCirculo = 0;
    std::cout << "Digite o raio do circulo: ";
    raioCirculo = lerEntrada();

    std::cout << "A área do circulo é: ";
    std::cout << areaCirculo(raioCirculo) << "\n";
}

int main()
{
    char opcaoUsuario = 0;

    // Lê opção do usuário
    std::cout << "Digite uma opção de área para calcular\n\tQ - Quadrado\n\tT - Triangulo\n\tC - Circulo\nOpcao = ";
    std::cin >> opcaoUsuario;

    if(opcaoUsuario == 'Q')
    {
        calculaAreaQuadrado();
    }
    else if(opcaoUsuario == 'T')
    {
        calculaAreaTriangulo();
    }
    else if(opcaoUsuario == 'C')
    {
        calculaAreaCirculo();
    }
    else
    {
        std::cout << "\nOpção inválida! Use Q, T ou C\n";
    }
    return 0;
}
----

Dessa forma, a função *main* fica muito menor e é facil de descobrir o que o
código faz só de ler ele. Basta, agora, entender como uma função funciona, a
sintaxe é a seguinte.

[source, cpp, linenums, highlight]
----
//tipoDoRetorno nomeDaFunção(Argumentos)
//{

//}
// Exemplos:
int fun1() // Uma função que retorna um int e não possui nenhum argumento
{
}

char fun2(int a) // Uma função que retorna um char e tem um int como argumento
{
}

void fun3(char a, int b) // Uma função que não retorna nada, tem um argumento do tipo char e um do tipo int
{
}
----

* O tipo do retorno pode ser qualquer coisa, como, um dos tipos vistos
  anteriormente.
* O nome da função também pode ser qualquer um, é por ele que você irá
  chamar/utilizar a função no seu código.
* Cada funçao pode ter infinitos argumentos, que são dados que a função precisa
  para realizar a sua tarefa, por exemplo, a função que calcula a área de um
  quadrado precisa saber qual o valor do lado do quadrado para fazer a conta.
  Dentro da função os argumentos se comportam como se fossem variáveis locais.
  É importante ressaltar que, a ordem com que os argumentos são passados na
  chamada da função, deve ser a mesma ordem com que os argumentos foram
  definidos, por exemplo:

[source, cpp, linenums, highlight]
----
void umaFuncaoQualquer(char arg1, int arg2, bool arg3)
{
    // Faz alguma coisa
}

int main()
{
    umaFuncaoQualquer('c', 2, false); // está correto
    // umaFuncaoQualquer(2, 'c', false); // errado
    // umaFuncaoQualquer(false, 2, 'c'); // errado
    // umaFuncaoQualquer(false, 'c', 2); // errado
    return 0;
}
----

E caso os argumentos tenham o mesmo tipo o cuidado deve ser redobrado, por
exemplo, no caso da função que calcula a área do triângulo:

[source, cpp, linenums, highlight]
----
float areaTriangulo(int base, int altura)
{
    return base * altura / 2.0;
}

int main()
{
    // b = base, h = altura
    int b = 10, h = 3;
    // Nada impede que eu inverta a ordem, porém, dentro da função os valore
    // esperados estarão invertidos
    // float area = areaTriangulo(h, b); // Não gera erro de compilação, mas vai gerar bugs
    // float area = areaTriangulo(b, b); // Não gera erro de compilação, mas vai gerar bugs
    // float area = areaTriangulo(h, h); // Não gera erro de compilação, mas vai gerar bugs
    float area = areaTriangulo(b, h); // Certo
    return 0;
}
----

Outro ponto importante é que, uma função não pode ser usada com uma quantidade
menor ou maior de argumentos, ela deve ser *igual* ao que foi colocado na sua
declaração.

Por fim, uma função só pode retornar *um único valor* ou *nenhum* (retorno do
tipo *void*), não é possível retornar 2, 3, 4... valores, para isso existem
outros meios.

NOTE: Para que a função *main* seja capaz de utilizar outras funções, essas
funções devem ser declaradas *antes* da função *main*, isso vale para qualquer
par de função. Por exemplo, observe que a função *lerEntrada()* é declarada
antes de qualquer função que faz o seu uso.
