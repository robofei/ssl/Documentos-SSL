// 1 - Inclusão de bibliotecas
#include <iostream> // Funções de entrada/saída da linguagem C++
#include <string>

// 2 - Função principal do programa
// int main()
// {
//     std::cout << "Divisão de inteiros: 5/2 = ";
//     std::cout << 5/2 << "\n";
//
//     char numChar = '9';
//     int valorChar = numChar - '0';
//     std::cout << "Conversão de caractere numérico para valor '" << numChar << "' = ";
//     std::cout << valorChar << "\n";
//
//     return 0;
// }

float areaQuadrado(int lado)
{
    return lado * lado;
}

float areaTriangulo(int base, int altura)
{
    // Note que foi utilizado o 2.0 para evitar que o resultado seja truncado
    return base * altura / 2.0;
}

float areaCirculo(int raio)
{
    return 3.1415 * raio * raio;
}

int lerEntrada()
{
    // 10 é o tamanho máximo do valor inserido pelo usuário
    char valor[10];
    std::cin >> valor;
    return std::stoi(valor); // Converte de string para int
}

void calculaAreaQuadrado()
{
    int ladoQuadrado = 0;
    std::cout << "Digite o lado do quadrado: ";
    ladoQuadrado = lerEntrada();

    std::cout << "A área do quadrado é: ";
    std::cout << areaQuadrado(ladoQuadrado) << "\n";
}

void calculaAreaTriangulo()
{
    int baseTriangulo = 0, alturaTriangulo = 0;
    std::cout << "Digite a base do triangulo: ";
    baseTriangulo = lerEntrada();
    std::cout << "Digite a altura do triangulo: ";
    alturaTriangulo = lerEntrada();

    std::cout << "A área do triangulo é: ";
    std::cout << areaTriangulo(baseTriangulo, alturaTriangulo) << "\n";
}

void calculaAreaCirculo()
{
    int raioCirculo = 0;
    std::cout << "Digite o raio do circulo: ";
    raioCirculo = lerEntrada();

    std::cout << "A área do circulo é: ";
    std::cout << areaCirculo(raioCirculo) << "\n";
}

int main()
{
    char opcaoUsuario = 0;

    // Lê opção do usuário
    std::cout << "Digite uma opção de área para calcular\n\tQ - Quadrado\n\tT - Triangulo\n\tC - Circulo\nOpcao = ";
    std::cin >> opcaoUsuario;

    if(opcaoUsuario == 'Q')
    {
        calculaAreaQuadrado();
    }
    else if(opcaoUsuario == 'T')
    {
        calculaAreaTriangulo();
    }
    else if(opcaoUsuario == 'C')
    {
        calculaAreaCirculo();
    }
    else
    {
        std::cout << "\nOpção inválida! Use Q, T ou C\n";
    }
    return 0;
}
