QT += core gui serialport network widgets

CONFIG += c++14 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        src/Field/ball.cpp \
        src/Field/field.cpp \
        src/Field/geometry.cpp \
        src/Field/robot.cpp \
        src/Field/team.cpp \
        src/General/general.cpp \
        src/SSLBase/sslbase.cpp \
        src/SSLBase/ssltraining.cpp

LIBS += \
    -lprotobuf

include(include/protofiles/proto_compile.pri)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/Field/ball.h \
    src/Field/field.h \
    src/Field/geometry.h \
    src/Field/robot.h \
    src/Field/team.h \
    src/General/general.h \
    src/SSLBase/sslbase.h
