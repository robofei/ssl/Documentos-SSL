#include <QApplication>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMainWindow>
#include <QDialog>

#include "src/SSLBase/sslbase.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QMainWindow *window = new QMainWindow;
    QPushButton *closeButton = new QPushButton("Fechar");
    window->setCentralWidget(closeButton);

    SSLBase ssl;
    QAbstractButton::connect(closeButton, &QPushButton::clicked,
                             &ssl, &SSLBase::vCloseApplication);

    window->show();

    ssl.vConnect();
    ssl.vStart();

    return a.exec();
}
