#include "ball.h"

Ball::Ball()
{
    ptPosition = QPoint(0, 0);
}

void Ball::vSetPosition(QPoint _pos)
{
    ptPosition = _pos;
}

QPoint Ball::ptGetPosition() const
{
    return ptPosition;
}

void Ball::vUpdateFromDetection(const SSL_DetectionFrame &_detection)
{
    if(_detection.balls_size() <= 0)
    {
        return;
    }

    ptPosition = QPoint(_detection.balls(0).x(), _detection.balls(0).y());
}
