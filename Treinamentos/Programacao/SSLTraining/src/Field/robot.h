#ifndef ROBOT_H
#define ROBOT_H

#include <QPoint>
#include <QVector2D>
#include <QVector3D>
#include <qmath.h>

#include "src/General/general.h"
#include "ssl_vision_detection.pb.h"

typedef enum KICK_TYPE
{
    DIRECT_KICK = 0, /**< Chute direto. */
    CHIP_KICK = 1    /**< Chip kick/Cavadinha. */
}KICK_TYPE;

typedef struct ROBOT_KICK
{
    KICK_TYPE type; /**< Tipo de chute. @see KICK_TYPE.*/
    float speed; /**< Velocidade do chute em m/s. */
}ROBOT_KICK;

constexpr QPoint OUT_OF_FIELD = QPoint(INT32_MAX, INT32_MAX);
#define PACKETS_TILL_MISSING 30

class Robot
{
    qint8 iID;
    QPoint ptPosition;
    float fAngle;

    float fLinearKp;
    float fAngularKp;
    QVector3D vt3dCommand;
    ROBOT_KICK kick;
    int iRollerSpeed;
    bool bBallSensor;

    bool bDetected;
    int iMissedDetections;

    QVector2D vt2dDecomposeVelocity(const QVector2D &_vel);
    float fPointToAngle(QPoint _pos);

public:
    Robot(qint8 _id = -1);

    /**
     * @brief Define o ID do robô.
     *
     * OBS: Essa função é de uso interno, NAO UTILIZE.
     *
     * @param _id
     */
    void vSetID(qint8 _id);
    /**
     * @brief Retorna o ID do robô.
     *
     * @param _id
     */
    int iGetID() const;
    /**
     * @brief Define a posicao do robô.
     *
     * OBS: Essa função é de uso interno, NAO UTILIZE.
     *
     * @param _pos
     */
    void vSetPosition(QPoint _pos);
    /**
     * @brief Retorna a posicao do robô [mm].
     *
     * @return
     */
    QPoint ptGetPosition() const;
    /**
     * @brief Define o angulo do robô.
     *
     * OBS: Essa função é de uso interno, NAO UTILIZE.
     *
     * @param _angle
     */
    void vSetAngle(float _angle);
    /**
     * @brief Retorna o ângulo do robô [rad].
     *
     * O ângulo do robô é definido de forma simétrica, isto é:
     *
     *             pi/2   pi/4
     *               |   /
     *               |  /
     *               | /
     *               |/
     * +- pi ------------------ 0
     *               |\
     *               | \
     *               |  \
     *               |   \
     *            -pi/2  -pi/4
     *
     * @return
     */
    float fGetAngle() const;

    /**
     * @brief Define o comando de velocidade do robô (Vx, Vy, Vw) [m/s].
     *
     * OBS: Essa função é de uso interno, NAO UTILIZE.
     *
     * @param _cmd
     */
    void vSetCommand(QVector3D _cmd);
    /**
     * @brief Retorna o comando de velocidade atual do robô [m/s].
     *
     * @return
     */
    QVector3D vt3dGetCommand() const;

    /**
     * @brief Faz o robô chutar.
     *
     * @param _kick - Pode ser DIRECT_KICK ou CHIP_KICK. @see ROBOT_KICK.
     * @param _kickSpeed - Velocidade do chute em m/s. O valor máximo é 6,5.
     */
    void vKick(ROBOT_KICK _kick);
    /**
     * @brief Faz o robô chutar.
     *
     * @param _kickSpeed
     * @param _type
     */
    void vKick(float _kickSpeed, KICK_TYPE _type);
    /**
     * @brief Retorna o comando de chute atual do robô.
     *
     * @return
     */
    ROBOT_KICK rkGetKick() const;

    /**
     * @brief Define a velocidade do roller, de 0 a 100.
     *
     * @param _speed
     */
    void vSetRollerSpeed(int _speed);
    /**
     * @brief Retorna a velocidade do roller [0 - 100].
     *
     * @return
     */
    int iGetRollerSpeed() const;

    /**
     * @brief Define o estado atual do sensor da bola.
     *
     * OBS: Essa função é de uso interno, NAO UTILIZE.
     *
     * @param _sensor
     */
    void vSetBallSensor(bool _sensor);
    /**
     * @brief Retorna o estado atual do sensor da bola, true ou false.
     *
     * @return
     */
    bool bGetBallSensor() const;

    /**
     * @brief Faz o robô se movimentar.
     *
     * @param _pos - Posição de destino do robô [mm, mm].
     * @param _angle - Angulo de destino do robô [rad].
     * @param _maxVelocity - Velocidade máxima do robô [m/s].
     */
    void vMoveTo(QPoint _pos, float _angle, float _maxVelocity);
    /**
     * @brief Faz o robô se movimentar.
     *
     * @param _pos - Posição de destino do robô [mm, mm].
     * @param _anglePos - Posição que o robô deve estar olhando [mm, mm].
     * @param _maxVelocity - Velocidade máxima do robô [m/s].
     */
    void vMoveTo(QPoint _pos, QPoint _anglePos, float _maxVelocity);
    /**
     * @brief Faz o robô somente rotacionar em seu próprio eixo.
     *
     * @param _angle - Angulo de destino do robô [rad].
     */
    void vRotateTo(float _angle);
    /**
     * @brief Faz o robô somente rotacionar em seu próprio eixo.
     *
     * @param _anglePos - Posição que o robô deve estar olhando [mm, mm].
     */
    void vRotateTo(QPoint _anglePos);

    /**
     * @brief Utilize esta função para calibrar a movimentação do robô.
     *
     * OBS: Essa função é de uso interno, NAO UTILIZE.
     *
     * @param _linear - Ganho da movimentação linear.
     * @param _angular - Ganho da movimentação angular.
     */
    void vSetKP(float _linear, float _angular);

    /**
     * @brief Função de uso interno.
     */
    void vStepDetection();
    /**
     * @brief Função de uso interno.
     *
     * @param _detection
     */
    void vUpdateFromDetection(const SSL_DetectionRobot &_detection);
};

#endif // ROBOT_H
