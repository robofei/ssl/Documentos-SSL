#ifndef GENERAL_H
#define GENERAL_H

#include <QVector2D>
#include <qmath.h>
#include <QDebug>

class General
{
public:
    General();

    /**
     * @brief Rotaciona um vetor em _degrees graus.
     *
     * @param _vector.
     * @param _degrees - Graus, sentido positivo anti horário.
     *
     * @return
     */
    static QVector2D vt2dRotateVector(const QVector2D &_vector, float _degrees);
    /**
     * @brief Rotaciona P2 em torno de P1 em _degrees graus.
     *
     * @param _P1.
     * @param _P2.
     * @param _degrees - Ângulo em graus, sentido positivo anti horário.
     *
     * @return
     */
    static QPoint vt2dRotateP2P1(const QPoint &_P1, const QPoint &_P2,
                                 float _degrees);
    /**
     * @brief Calcula o ângulo entre os vetores V1 e V2.
     *
     * @param _V1
     * @param _V2
     *
     * @return Ângulo em radianos.
     */
    static double dAngleV1V2(const QVector2D &_V1, const QVector2D &_V2);
};

#endif // GENERAL_H
