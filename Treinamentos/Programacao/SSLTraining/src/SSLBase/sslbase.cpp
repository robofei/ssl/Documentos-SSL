#include "sslbase.h"
#include <cstdlib>

SSLBase::SSLBase()
{
    udpVision = new QUdpSocket();
    udpTeam = new QUdpSocket();
    udpOpponent = new QUdpSocket();
    udpSimulationControl = new QUdpSocket();

    udpVision->bind(QHostAddress::AnyIPv4, VISION_PORT,
                    QUdpSocket::ShareAddress);

    tmrLoop = new QTimer();
    connect(tmrLoop, &QTimer::timeout, this, &SSLBase::vUpdate);

    bPacketsReceived = false;
    iInterfaceIndex = 1;
    iCycles = 0;
}

SSLBase::~SSLBase()
{

}

void SSLBase::vDiscoverInterface()
{
    if(!bPacketsReceived && iCycles * CYCLE_MS > 3000)
    {
        iCycles = 0;
        qInfo() << "[Info] Conectando ...";
        udpVision->disconnect(SIGNAL(readyRead()));
        iInterfaceIndex++;
        vConnect();
    }
}

void SSLBase::vSendData()
{
    QByteArray datagram = barrayCreateDatagram(&field.allies);
    if(udpTeam->writeDatagram(datagram, haSimulatorIP, iTeamPort) == -1)
    {
        qWarning() << "[Warning] Erro ao enviar dados para os robôs aliados!"
                   << qPrintable(udpTeam->errorString());
    }

    datagram = barrayCreateDatagram(&field.opponents);
    if(udpOpponent->writeDatagram(datagram, haSimulatorIP, iOpponentPort) == -1)
    {
        qWarning() << "[Warning] Erro ao enviar dados para os robôs oponentes!"
                   << qPrintable(udpOpponent->errorString());
    }
}

void SSLBase::vUpdateVisionData(const SSL_WrapperPacket &_visionData)
{
    //Atualiza as informacoes dos robos e da bola
    //Aqui sera detectado se o robo saiu da visao ou entao, caso ele esteja no campo sera adicionada sua posicao atual
    //no vetor utilizado pelo kalman para calcular sua posicao real, o mesmo vale para a bola.
    if(_visionData.has_detection())
    {
        field.allies.vUpdateDetection(_visionData.detection());
        field.opponents.vUpdateDetection(_visionData.detection());
        field.ball.vUpdateFromDetection(_visionData.detection());
    }

    //Recebe os dados de geometria do campo
    if(_visionData.has_geometry())
    {
        field.geometry.vUpdateFromDetection(_visionData.geometry());
    }
}

QByteArray SSLBase::barrayCreateDatagram(const SSLTeam *_team)
{
    RobotControl control;
    for(const QSharedPointer<Robot> &robo : qAsConst(_team->robots))
    {
        // Adiciona um novo comando ao pacote `control`
        RobotCommand* command = control.add_robot_commands();
        MoveLocalVelocity* localVelocity = command->mutable_move_command()->mutable_local_velocity();

        command->set_id(robo->iGetID());

        const QVector3D robotCmd = robo->vt3dGetCommand();
        localVelocity->set_forward(robotCmd.y());
        localVelocity->set_left(-robotCmd.x()); // No GrSim é invertido
        localVelocity->set_angular(robotCmd.z());

        const ROBOT_KICK robotKick = robo->rkGetKick();
        command->set_kick_speed(robotKick.speed);
        command->set_kick_angle(robotKick.type == CHIP_KICK ? 45 : 0);
        command->set_dribbler_speed(robo->iGetRollerSpeed() * 100e3);
    }

    QByteArray datagram;
    datagram.resize(control.ByteSizeLong());
    control.SerializeToArray(datagram.data(), datagram.size());

    return datagram;
}

void SSLBase::vSetRobotPosition(int _teamID, qint8 _id, QPoint _pos)
{
    if(_teamID != BLUE_ID && _teamID != YELLOW_ID)
    {
        qCritical() << "[Fatal] _teamID deve ser BLUE_ID ou YELLOW_ID!";
    }

    SimulatorCommand command;
    TeleportRobot* robot = command.mutable_control()->add_teleport_robot();

    RobotId* robotData = robot->mutable_id();
    robotData->set_id(_id);
    robotData->set_team(_teamID == BLUE_ID ? Team::BLUE : Team::YELLOW);

    robot->set_x(_pos.x() / 1e3);
    robot->set_y(_pos.y() / 1e3);
    robot->set_orientation(0);

    robot->set_by_force(true);

    QByteArray datagram;
    datagram.resize(command.ByteSizeLong());
    command.SerializeToArray(datagram.data(), datagram.size());

    if(udpSimulationControl->writeDatagram(datagram, haSimulatorIP, CONTROL_PORT) == -1)
    {
        qWarning() << "[Warning] Erro ao tentar controlar a simulação!"
                   << qPrintable(udpSimulationControl->errorString());
    }
}

void SSLBase::vConnect()
{
    QList<QNetworkInterface> availableInterfaces = QNetworkInterface::allInterfaces();
    if(iInterfaceIndex >= availableInterfaces.size())
    {
        qCritical() << "[Fatal] Não foram recebidos pacotes em nenhuma interface!";
        exit(-1);
    }

    const QNetworkInterface interface = availableInterfaces.at(iInterfaceIndex);

    if(udpVision->joinMulticastGroup(QHostAddress(VISION_IP), interface))
    {
        // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
        bPacketsReceived = false;
        connect(udpVision, &QUdpSocket::readyRead,
                this, &SSLBase::vParseVisionPacket);
        qInfo() << "[Info] Conectado! IP:Porta =" << VISION_IP << ":" << VISION_PORT
                << " | Interface:" << interface.humanReadableName();
    }
    else
    {
        qCritical() << "[Erro] Não foi possível se conectar com a visão! "
                    << "IP: " << VISION_IP << " | Interface: "
                    << interface.humanReadableName();
        return;
    }


    const QNetworkAddressEntry addrEntry = interface.addressEntries().constFirst();
    haSimulatorIP = addrEntry.broadcast();
    iTeamPort = BLUE_PORT;
    iOpponentPort = YELLOW_PORT;

    if(field.allies.getTeamColor().iID != BLUE_ID)
    {
        iTeamPort = YELLOW_PORT;
        iOpponentPort = BLUE_PORT;
    }

    qInfo() << QString("[Simulador] Conectando em %1 @ %2 | Time Aliado(%3): %4 | Time Oponente(%5): %6")
        .arg(haSimulatorIP.toString())
        .arg(interface.humanReadableName())
        .arg(iTeamPort)
        .arg(field.allies.getTeamColor().strName)
        .arg(iOpponentPort)
        .arg(field.opponents.getTeamColor().strName);

    // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
    connect(udpTeam, &QUdpSocket::readyRead,
            this, &SSLBase::vParseRobotFeedback);
    connect(udpOpponent, &QUdpSocket::readyRead,
            this, &SSLBase::vParseOpponentFeedback);
}

void SSLBase::vStart()
{
    tmrLoop->start(CYCLE_MS);
    qInfo() << "[Info] Loop" << CYCLE_MS << "ms";
}

void SSLBase::vCloseApplication()
{
    qInfo() << "[Info] Fechando...";
    udpTeam->disconnectFromHost();
    udpOpponent->disconnectFromHost();
    udpVision->disconnectFromHost();
    udpSimulationControl->disconnectFromHost();
    exit(0);
}

void SSLBase::vParseVisionPacket()
{
    if(!bPacketsReceived)
    {
        bPacketsReceived = true;
        qInfo() << "[Info] Pacote recebido!";
    }

    while(udpVision->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpVision->pendingDatagramSize());
        datagram.fill(0, udpVision->pendingDatagramSize());
        udpVision->readDatagram(datagram.data(), datagram.size());

        SSL_WrapperPacket visionData;
        visionData.ParseFromArray(datagram, datagram.size());

        if(visionData.ByteSizeLong() > 0)
        {
            vUpdateVisionData(visionData);
        }
    }
}

void SSLBase::vParseRobotFeedback()
{
    while(udpTeam->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpTeam->pendingDatagramSize());
        datagram.fill(0, udpTeam->pendingDatagramSize());
        udpTeam->readDatagram(datagram.data(), datagram.size());

        RobotControlResponse response;
        response.ParseFromArray(datagram, datagram.size());

        if(response.ByteSizeLong() > 0)
        {
            for(int i = 0; i < response.feedback_size(); ++i)
            {
                const RobotFeedback& feedback = response.feedback(i);
                if(feedback.has_id() && feedback.has_dribbler_ball_contact())
                {
                    const qint8 robotID = static_cast<qint8>(feedback.id());
                    field.allies.robots.value(robotID)->vSetBallSensor(feedback.dribbler_ball_contact());
                }
            }
        }
    }
}

void SSLBase::vParseOpponentFeedback()
{
    while(udpOpponent->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpOpponent->pendingDatagramSize());
        datagram.fill(0, udpOpponent->pendingDatagramSize());
        udpOpponent->readDatagram(datagram.data(), datagram.size());

        RobotControlResponse response;
        response.ParseFromArray(datagram, datagram.size());

        if(response.ByteSizeLong() > 0)
        {
            for(int i = 0; i < response.feedback_size(); ++i)
            {
                const RobotFeedback& feedback = response.feedback(i);
                if(feedback.has_id() && feedback.has_dribbler_ball_contact())
                {
                    const qint8 robotID = static_cast<qint8>(feedback.id());
                    field.opponents.robots.value(robotID)->vSetBallSensor(feedback.dribbler_ball_contact());
                }
            }
        }
    }
}

void SSLBase::vUpdate()
{
    vDiscoverInterface();
    ++iCycles;

    vMain();

    vSendData();
}
