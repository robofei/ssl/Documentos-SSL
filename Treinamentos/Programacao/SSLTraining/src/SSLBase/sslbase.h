#ifndef SSLBASE_H
#define SSLBASE_H

#include <QObject>
#include <QNetworkInterface>
#include <QUdpSocket>
#include <QTimer>
#include <QDebug>

#include "src/Field/field.h"

#include "ssl_simulation_robot_control.pb.h"
#include "ssl_simulation_robot_feedback.pb.h"
#include "ssl_simulation_control.pb.h"
#include "ssl_simulation_error.pb.h"
#include "ssl_gc_common.pb.h"
#include "ssl_simulation_config.pb.h"
#include "ssl_wrapper.pb.h"
#include "ssl_vision_geometry.pb.h"
#include "ssl_vision_detection.pb.h"

//-----------------------------------------------------------------------------
// Para evitar conflito com os simuladores de outros membros dentro do lab,
// configure aqui as portas de utilização do grSim. Pode ser atribuído qualquer
// valor, desde que esteja igual aqui e no simulador.

#define VISION_PORT 10020      // Nome dentro do grSim: Vision multicast port
#define VISION_IP "224.5.23.2" // Não é necessário alterar
#define BLUE_PORT 10301        // Nome dentro do grSim: Blue team control port
#define YELLOW_PORT 10302      // Nome dentro do grSim: Yellow team control port
#define CONTROL_PORT 10300     // Nome dentro do grSim: Simulation control port
//-----------------------------------------------------------------------------

#define CYCLE_MS 1000/60 // 60 Hz

class SSLBase : public QObject
{
    Q_OBJECT

    QUdpSocket *udpVision;
    QUdpSocket *udpTeam;
    QUdpSocket *udpOpponent;
    QUdpSocket *udpSimulationControl;

    QHostAddress haSimulatorIP;
    int iTeamPort;
    int iOpponentPort;
    QTimer *tmrLoop;

    bool bPacketsReceived;
    int iInterfaceIndex;
    int iCycles;

    Field field;

    // ============= Se necessário, crie seus atributos aqui \/ ===============

    // ============= Se necessário, crie seus atributos aqui /\ ===============

    void vDiscoverInterface();
    void vSendData();
    void vUpdateVisionData(const SSL_WrapperPacket &_visionData);
    QByteArray barrayCreateDatagram(const SSLTeam *_team);

    void vSetRobotPosition(int _teamID, qint8 _id, QPoint _pos);

    // ============= Se necessário, crie seus métodos aqui \/ =================

    // ============= Se necessário, crie seus métodos aqui /\ =================

public:
    SSLBase();
    ~SSLBase();

    void vConnect();
    void vStart();
    void vMain();

public slots:
    void vCloseApplication();

private slots:
    void vParseVisionPacket();
    void vParseRobotFeedback();
    void vParseOpponentFeedback();
    void vUpdate();
};

#endif // SSLBASE_H
