#include "src/SSLBase/sslbase.h"

void SSLBase::vMain()
{
    // Para acessar os objetos do campo (robôs, bola, geometria) utilize o
    // atributo field, por exemplo, para fazer o robô 0 ir até o ponto (0, 0)
    // mm, com ângulo pi rad, à uma velocidade máxima de 2m/s utilize:
    //
    //     field.allies.robots.value(2)->vMoveTo({0, 0}, M_PI, 2);
    //
    // Caso necessário, também é possível manipular o time oponente:
    //
    //     field.opponents.robots.value(2)->vMoveTo({1000, 1000}, M_PI, 2);
}
